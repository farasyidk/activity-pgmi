<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrestasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('prestasi', function (Blueprint $table) {
          $table->increments('id');
          $table->string('lomba');
          $table->text('peserta');
          $table->string('sifat');
          $table->string('jenjang');
          $table->text('content');
          $table->date('tanggal');
          $table->text('gambar');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestasi');
    }
}
