<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Blog;
use App\Models\Nav;
use App\Models\Slider;
use App\Models\Prestasi;
use App\Models\Kategori;
use App\Models\Event;
use App\Models\Gallery;

class BlogController extends Controller
{
    public function index($bhs = 'id') {
        if ($bhs == 'id') {
            $bhsa = 0; $cpt = 'captionId';
            $kat = 'nama'; $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1; $cpt = 'captionEn';
            $jdl = 'judul_en'; $kat = 'namaEn';
        } else {
            return redirect('/id');
        }
        $navs = navs($bhs);
        $popBlogs = Blog::join('kategories','id_kategori', 'kategori')->join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->orderBy('blogs.view', 'desc')->limit(3)->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->get();
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhs]])->get();
        $newBlogs = Blog::join('kategories','id_kategori', 'kategori')->join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->orderBy('blogs.updated_at', 'desc')->limit(3)->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->get();
        $extBlogs = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->orderBy('blogs.tanggal', 'desc')->limit(4)->where([['blogs.bahasa',$bhsa],['kategories.gol','Ekstra'],['blogs.aktif','1']])->get();
        $events = Prestasi::where([['aktif', '1'],['bhs',$bhsa]])->orderBy('tanggal', 'desc')->get();
        $slider = Slider::join('galleries', 'id_gambar','id_gallery')->select('gambar',$cpt)->get()->toArray();
        //$slider = slider::all();
        return view('layouts/home', compact(['aktifBlogs','kat','cpt','slider','url','bhs','navs','navs', 'popBlogs','newBlogs','extBlogs','events','jdl']));
    }
    public function prestasi($bhs='id',$slug) {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        } else {
            $bhsa = 0;
            $jdl = 'judul';
        }
        $navs = navs($bhs);
        $post = Prestasi::join('galleries', 'prestasi.img', '=', 'galleries.id_gallery')->join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where([['prestasi.bhs',$bhsa],['prestasi.slug', $slug],['prestasi.aktif','1']])->get();
        $terkaits = Prestasi::join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where([['prestasi.aktif','1'],['prestasi.bhs', $bhsa],['prestasi.slug','!=',$slug],['kategories.nama', $post[0]['nama']]])->orderBy('prestasi.view', 'desc')->limit(4)->get()->toArray();
        $newPost = Prestasi::join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where([['prestasi.bhs', $bhsa],['prestasi.aktif','1']])->orderBy('prestasi.created_at', 'desc')->limit(4)->get();
        $popBlogs = Prestasi::join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where([['prestasi.bhs', $bhsa],['prestasi.aktif','1']])->orderBy('prestasi.view', 'desc')->limit(4)->get();
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhsa]])->get();
        $tambah = $post[0]['view'] + 1;
        $upView = Prestasi::where('slug', $slug)->update(['view'=>$tambah]);
        //dd($bhs);
        return view('layouts/all-prestasi', compact(['bhs','navs','post','terkaits','newPost','popBlogs','aktifBlogs','jdl']));
    }
    public function blogs($bhs='id',$page,Request $request) {

        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        } else {
            return redirect('/id');
        }
        $navs = navs($bhs);
        $popBlogs = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->orderBy('blogs.view', 'desc')->limit(4)->get();
        if ($page == 'prestasi') {
            $blogs = Prestasi::join('galleries', 'prestasi.img', '=', 'galleries.id_gallery')->join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->orderBy('prestasi.tanggal', 'desc')->where([['prestasi.bhs',$bhsa],['prestasi.aktif','1']])->paginate(6);
        } else {
            $blogs = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->orderBy('blogs.tanggal', 'desc')->where([['blogs.bahasa',$bhsa],['kategories.nama', $page],['blogs.aktif','1']])->orWhere('kategories.gol', $page)->paginate(6);
        }
        //dd($blogs[0]['judul']);
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhs]])->get();
        if(!empty($blogs[0]['gol'])) $url = $blogs[0]['gol']; else $url = $blogs[0]['nama'];

        if ($request->ajax()) {
            return view ('layouts/dataBlogs', compact(['url','bhs','navs','blogs','popBlogs','events','jdl']));
        }
        if (!$blogs) {
            return view('errors/404');
        }
        return view('layouts/blogs', compact(['url','bhs','navs','blogs','popBlogs','aktifBlogs','jdl']));
    }
    public function login($bhs='id') {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        } else {
            $bhsa = 0;
            $jdl = 'judul';
        }
        $navs = navs($bhs);

        return view('layouts/login', compact('navs','bhs'));
    }
    public function masuk($bhs='id') {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        } else {
            $bhsa = 0;
            $jdl = 'judul';
        }
//        Session::has('mhs', 'maha');
        $navs = navs($bhs);
        return view('mhs/home', compact('navs','bhs'));
    }
    public function post($bhs = 'id',$slug) {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        } else {
            $bhsa = 0;
            $jdl = 'judul';
        }
        $navs = navs($bhs);
        $post = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.bahasa',$bhsa],['blogs.slug', $slug],['blogs.aktif','1']])->get();
        $terkaits = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.aktif','1'],['blogs.bahasa', $bhsa],['blogs.slug','!=',$slug],['kategories.gol', $post[0]['gol']]])->orWhere('kategories.gol', $post[0]['gol'])->orderBy('blogs.view', 'desc')->limit(4)->get();
        $newPost = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['bahasa', $bhsa],['blogs.aktif','1']])->orderBy('blogs.tanggal', 'desc')->limit(4)->get();
        $popBlogs = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->orderBy('blogs.view', 'desc')->limit(4)->get();
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhs]])->get();
        //dd($terkaits);
        $tambah = $post[0]['view'] + 1;
        $upView = Blog::where('slug', $slug)->update(['view'=>$tambah]);
        if (!empty($blogs[0]['gol'])) {
            $url = $post[0]['gol'];
            $url[1] = $popBlogs[0]['gol'];
        } else {
            $url = $post[0]['nama'];
        }
        dd($terkaits);
        if (!$post) {
            return view('errors/404');
        }//dd($popBlogs);
        return view('layouts/post', compact(['url','bhs','navs','post','terkaits','newPost','popBlogs','aktifBlogs','jdl']));
    }
    public function guide($bhs="id") {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        }
        $navs = navs($bhs);
        $popBlogs = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->orderBy('blogs.view', 'desc')->limit(4)->get();
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhs]])->get();

        return view('layouts/guide', compact(['navs','jdl','bhs','popBlogs','aktifBlogs']));
    }
    public function search($bhs='id',Request $request) {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        }
        if($request->has('search')){
      		//$blog = Blog::query("select * from ")->get();
            //$resul = $blog->search($request->search);
        } else {
      		//$blogs = Blog::paginate(6);
      	}
        //dd($resul);
        $popBlogs = Blog::join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where([['blogs.bahasa', $bhsa],['blogs.aktif','1']])->orderBy('blogs.view', 'desc')->limit(4)->get();
        $navs = navs($bhs);
        $blogs = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->where('judul', 'like', "%$request->search%")->orWhere('content', 'like', "%$request->search%")
                 ->where([['bahasa',0],['aktif','1']])->get();
        $blogs2 = Prestasi::join('galleries', 'prestasi.img', '=', 'galleries.id_gallery')->join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->where([['prestasi.bhs',$bhsa],['prestasi.aktif','1'],['prestasi.judul', 'like', "%$request->search%"]])->orWhere('prestasi.content', 'like', "%$request->search%")->get();
        //$blogs = Blog::search($request->search)->paginate(10);
        $aktifBlogs = DB::table('blogs')->select(DB::raw('blogs.from, count(blogs.from) as maha'))->groupBy('blogs.from')->orderBy('maha','desc')->where([['blogs.aktif','1'],['blogs.bahasa',$bhs]])->get();
        //dd($blogs." ".$blogs2);
        return view('layouts/search', compact(['blogs2','url','bhs','navs','blogs','popBlogs','aktifBlogs','jdl']));
    }
    public function gallery($bhs ='id') {
        if ($bhs == 'id') {
            $bhsa = 0;
            $jdl = 'judul';
        } elseif ($bhs == 'en') {
            $bhsa = 1;
            $jdl = 'judul_en';
        }
        //dd('ss');
        $navs = navs($bhs);
        $kategori = Kategori::all();
        $gallery = Blog::join('galleries', 'blogs.img', '=', 'galleries.id_gallery')->join('kategories', 'blogs.kategori', '=', 'kategories.id_kategori')->orderBy('blogs.created_at','desc')->where([['blogs.bahasa',$bhsa],['blogs.aktif','1']])->get();
        $prestasi = Prestasi::join('galleries', 'prestasi.img', '=', 'galleries.id_gallery')->join('kategories', 'prestasi.kategori', '=', 'kategories.id_kategori')->orderBy('prestasi.tanggal','desc')->where([['prestasi.bhs',$bhsa],['prestasi.aktif','1']])->get();
        $vImage = Gallery::all();
        //dd($gallery);
        $a=0;

        for ($i=0; $i < count($prestasi); $i++) {
            if ($prestasi[$i]['imgDesc'] != "") {
                $tampil = explode(",", $prestasi[$i]['imgDesc']);
                    for ($e=0; $e < count($tampil); $e++) {
                        $id[$a] = $tampil[$e]; $a++;
                    }
                }

            $id[$a] = $prestasi[$i]['img']; $a++;
        }
        //dd($tampil);

        for ($g=0; $g < count($gallery); $g++) {
            if (!empty($gallery[$g]['imgContent'])) {
                $tampil = explode(",", $gallery[$g]['imgContent']);
                    for ($e=0; $e < count($tampil); $e++) {
                        $id[$a] = $tampil[$e]; $a++;
                    }

            }
            $id[$a] = $gallery[$g]['img']; $a++;
        }
        //dd($prestasi);
        return view('layouts/gallery', compact(['kategori','bhs','navs','gallery','vImage','id']));
    }
    /*public function insert(Request request) {
        //create
        $tambah = Blog::create('judul' => $request, 'content' => $request, 'kategori'=>$request, 'bahasa'=>$request, 'img'=>$request,'view'=>$request,'aktif'=>$request,'from'=>$request);
        if ($tambah) {
            return redirect('/');
        }
    }

    public function update(Request $request, $id) {
        //update
        $update = Blog::find($id)->update(['judul' => $request, 'content' => $request, 'kategori'=>$request, 'bahasa'=>$request, 'img'=>$request,'view'=>$request,'aktif'=>$request,'from'=>$request]);
        if ($update) {
            return redirect('/');
        }
    }

    public function destroy($id) {
        //SoftDelete
        Blog::find($id)->delete();
        return redirect('/');
    }*/
}
