-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 12, 2017 at 08:36 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id3950879_activity`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `img` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgContent` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` bigint(20) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `judul`, `slug`, `content`, `kategori`, `bahasa`, `tanggal`, `img`, `imgContent`, `view`, `aktif`, `from`, `baca`, `created_at`, `updated_at`, `deleted_at`) VALUES
(72, 'KEGIATAN MAGANG PKL DAN UJI COBA SOAL', 'kegiatan-magang-pkl-dan-uji-coba-soal', '<span id=\"docs-internal-guid-99d16ffa-4267-df22-2880-25b9c12035ec\"><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'>Mahasiswa PGMI semester 5 telah melaksanakan Magang 1, PKL (Praktik Kerja Lapangan), dan Uji Coba soal. Pelaksanaaan kegiatan tersebut berkaitan dengan tugas mata kuliah dan observasi. Hal ini berguna untuk melatih kemampuan mahasiswa dalam melihat sekolah, baik dari sisi proses pembelajaran sampai pada proses manajerial sekolah. Selain itu manfaat dari kegiatan tersebut adalah untuk melatih mahasiswa agar mampu berkomunikasi dengan sekolah, menjalin komunikasi dengan kepala sekolah, guru, dan murid. Hal lain dari kegiatan tersebut guna &Acirc;&nbsp;guna meningkatkan komunikasi antara Prodi, sekolah, dan mahasiswa itu sendiri, sehingga terjalin kerjasama. hubungan baik yang dijalin akan sangat menguntungkan untuk seluruh pihak di masa mendatang. </span></p><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><img data-filename=\"post1.jpg\" style=\"width: 473.5px;\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf50c3612.jpeg\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'></span></p><br><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><img data-filename=\"pos2.jpg\" style=\"width: 473.5px;\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf50dc93b.jpeg\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'></span></p><br><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><img data-filename=\"post3.jpg\" style=\"width: 473.5px;\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf50ed4f4.jpeg\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'></span></p><br><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><img style=\"width: 473.5px;\" data-filename=\"post4.jpg\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf5117729.jpeg\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'></span></p><br><p dir=\"ltr\" style=\"line-height:1.7999999999999998;margin-top:0pt;margin-bottom:0pt;text-align: justify;\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'></span></p><div><img style=\"width: 473.5px;\" data-filename=\"post5.jpg\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf5138942.jpeg\"><span style=\'font-size: 12pt; font-family: \"Times New Roman\"; background-color: transparent; vertical-align: baseline; white-space: pre-wrap;\'><br></span></div></span>\n', 'K0ppm', 0, NULL, '171', '', 10, 1, '15480061', 1, '2017-12-10 22:04:01', '2017-12-11 04:24:34', NULL),
(73, 'TOP TRADE ACTIVITIES AND TRIAL TESTS', 'top-trade-activities-and-trial-tests', '<p>Students of PGMI 5th semester have conducted Internship 1, PKL (Field Work Practice), and Trial about. Implementation of these activities related to course assignment and observation. This is useful for training students\' ability to see the school, both from the learning process to the school managerial process. In addition the benefits of such activities are to train students to be able to communicate with schools, establish communication with principals, teachers, and students. Another thing of these activities in order to improve communication between Prodi, school, and students themselves, so that cooperation is established. well-established relationship will be very beneficial for all parties in the future.<p><img data-filename=\"post1.jpg\" style=\"width: 473.5px;\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf5180396.jpeg\"></p><p><img style=\"width: 473.5px;\" data-filename=\"pos2.jpg\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf5199163.jpeg\"><br></p><p><img data-filename=\"post3.jpg\" style=\"width: 473.5px;\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf51b6abb.jpeg\"></p><p><img data-filename=\"post4.jpg\" style=\"width: 473.5px; float: left;\" class=\"note-float-left\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf51e3e45.jpeg\"><br></p><p><img style=\"width: 100%; float: right;\" data-filename=\"post5.jpg\" class=\"note-float-right\" src=\"https://activity-pgmi.000webhostapp.com/assets/img/blogs/5a2daf521a9e5.jpeg\"><br></p><p><br></p><p>                                  \r\n                               </p></p>\n', 'K0ppm', 1, NULL, '171', '', 0, 1, '15480061', 1, '2017-12-10 22:04:02', '2017-12-11 00:02:16', NULL),
(74, 'KTP Dance dengan Tari Denok dari Semarang', 'ktp-dance-dengan-tari-denok-dari-semarang', '<span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">KTP (Kreasi Tari PGMI) Dance mendapat undangan untuk mengisi acara Seminar Nasional dan<br style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Opening Ceremony Kompetisi Debat Konstitusi Mahasiswa antar Perguruan</span><br style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Tinggi Se-Indonesia 2017 Tingkat Regional Tengah. Acara ini</span><br style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">diselenggrakan oleh Fakultas Syariah dan Hukum UIN Suka Yogyakarta</span><br style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">pada hari selasa, 1 Agustus 2017 di CH lt.1 UIN Suka</span></span>\n', 'K0klta', 0, '2017-08-01', '182', '', 3, 1, '14480006', 1, '2017-12-10 22:33:33', '2017-12-11 00:48:32', NULL),
(75, 'KTP Dance with Denok Dance from Semarang', 'ktp-dance-with-denok-dance-from-semarang', '<pre class=\"tw-data-text tw-ta tw-text-small\" data-placeholder=\"Translation\" id=\"tw-target-text\" data-fulltext=\"\" dir=\"ltr\" style=\"unicode-bidi: isolate; background-color: rgb(255, 255, 255); border-width: initial; border-style: none; border-color: initial; padding: 0px 0.14em 0px 0px; position: relative; margin-bottom: 0px; resize: none; font-family: inherit; overflow: hidden; width: 276px; white-space: pre-wrap; color: rgb(33, 33, 33); height: 264px; font-size: 16px !important; line-height: 24px !important;\"><span lang=\"en\">KTP (Kreasi Tari PGMI) Dance received an invitation to fill National Seminar and event\r\nOpening Ceremony Competition Debate Constitution Students between Universities\r\nHigh Se-Indonesia 2017 Middle Regional Level. This event\r\norganized by the Faculty of Sharia and Law of UIN Suka Yogyakarta\r\non Tuesday, August 1, 2017 at CH lt.1 UIN Likes</span></pre>\n', 'K0klta', 1, '2017-08-01', '182', '', 0, 1, '14480006', 1, '2017-12-10 22:33:33', '2017-12-10 22:33:33', NULL),
(76, 'KTP Dance dalam Acara International Seminar Value Based Learning for Wonderful Children', 'ktp-dance-dalam-acara-international-seminar-value-based-learning-for-wonderful-children', '<div>KTP Dance mengisi acara dalam International Seminar Value Based<div>Learning for Wonderful Childern yang diselenggarakan oleh PGMI FITK</div><div>UIN Sunan Kalijaga pada tanggal 22 November 2017 di CH lantai 2 UIN Suka dengan tari</div><div>Ya Saman dari Palembang. Acara ini terbuka untuk umum.</div></div>\n', 'K0klta', 0, NULL, '183', '', 3, 1, '14480006', 1, '2017-12-10 23:13:15', '2017-12-11 00:48:46', NULL),
(77, 'KTP Dance in International Seminar Value Based Learning for Wonderful Children', 'ktp-dance-in-international-seminar-value-based-learning-for-wonderful-children', '<p>KTP Dance fills the event in International Seminar Value Based<p>Learning for Wonderful Childern organized by PGMI FITK</p><p>UIN Sunan Kalijaga on 22 November 2017 on the 2nd floor CH UIN Likes with dance</p><p>Ya Saman from Palembang. This event is open to the public.</p></p>\n', 'K0klta', 1, '2017-11-22', '183', '', 0, 1, '14480006', 1, '2017-12-10 23:13:15', '2017-12-10 23:13:15', NULL),
(78, 'KTP Dance dalam Acara AUN QA UIN Sunan Kalijaga', 'ktp-dance-dalam-acara-aun-qa-uin-sunan-kalijaga', '<div>KTP (Kreasi Tari PGMI) Dance mengisi salah satu rangkaian acara AUN QA UIN Sunan Kalijaga<div>Yogyakarta pada hari Selasa, 04 April 2017 di CH lantai 2 UIN Sunan Kalijaga yang</div><div>dihadiri oleh beberapa assesor dari luar negeri. Dengan membawakan</div><div>Tari Denok dari Semarang dan Tari Puji Astuti dari Jawa.</div></div>\n', 'K0klta', 0, '2017-04-04', '184', '', 0, 1, '14480006', 1, '2017-12-10 23:16:54', '2017-12-10 23:16:54', NULL),
(79, 'KTP Dance in AUN QA UIN Sunan Kalijaga', 'ktp-dance-in-aun-qa-uin-sunan-kalijaga', '<div>KTP Dance filled one of AUN QA UIN Sunan Kalijaga event series<div>Yogyakarta on Tuesday, April 4, 2017 at the 2nd floor of UIN Sunan Kalijaga</div><div>attended by several assesors from abroad. By bringing</div><div>Denok Dance from Semarang and Tari Puji Astuti from Java.</div></div>\n', 'K0klta', 1, '2017-04-04', '184', '', 0, 1, '14480006', 1, '2017-12-10 23:16:54', '2017-12-10 23:16:54', NULL),
(80, 'KAJIAN ISLAMI PGMI DALAM RANGKA PERINGATAN MAULID NABI MUHAMMAD SAW 1939 H', 'kajian-islami-pgmi-dalam-rangka-peringatan-maulid-nabi-muhammad-saw-1939-h', '<p class=\"MsoNormal\" style=\"text-indent:36.0pt;line-height:200%\"><span lang=\"IN\" style=\"font-size:12.0pt;line-height:200%;font-family:\" times new roman mso-fareast-font-family:>12 Rabiul Awwal merupakan hari yang\r\ndinanti-nanti umat Islam dalam merayakan hari besar kekasih agung, Habibana\r\nMuhammad Saw. pada bulan rabiul awal, banyak umat islam yang merayakan milad\r\nMuhammad SAW dengan beberapa kegiatan yang bermanfaat, seperti pengajian akbar,\r\npembacaan maulid, dan shalawat bersama. <p></p></span><p class=\"MsoNormal\" style=\"text-indent:36.0pt;line-height:200%\"><span lang=\"IN\" style=\"font-size:12.0pt;line-height:200%;font-family:\" times new roman mso-fareast-font-family:>Kajian islami pgmi (KIP) merupakan\r\nsalah satu agenda dalam menyongsong peringatan maulid nabi Muhammad SAW. KIP\r\nmerupakan majelis pembacaan maulid dan shalawat yang diselenggarakan oleh grup\r\nhadroh al-Ibtidaiyah PGMI, fakultas ilmu tarbiyah dan keguruan UIN Sunan\r\nKalijaga Yogyakarta. Kegiatan ini sudah berjalan selama 3 tahun terakhir. <p></p></span></p><p class=\"MsoNormal\" style=\"text-indent:36.0pt;line-height:200%\"><span lang=\"IN\" style=\"font-size:12.0pt;line-height:200%;font-family:\" times new roman mso-fareast-font-family:>Pada tahun ini, KIP dilaksanakan\r\npada hari sabtu 09 desember 2017 tepatnya 20 Rabiul Awwal 1939 H dirumah salah\r\nsatu personel hadroh, yaitu mas Arif Mustofa. Kajian ini dihadiri oleh 30\r\npeserta dari personel hadroh al-Ibtidaiyah dan beberapa dari mahasiswa PGMI non\r\nhadroh, mahasiswa MPI dan PAI. Tidak ketinggalan juga acara ini didukung dan\r\ndihadiri oleh beliau bapak Drs. Nur Hidayat, M.A selaku pembina hadroh\r\nal-Ibtidaiyah.<p></p></span></p><span lang=\"IN\" style=\"font-size:12.0pt;line-height:107%;font-family:\" times new roman mso-fareast-font-family: en-us>Adapun kegiatan KIP secara garis besar yakni\r\npembacaan maulid dan shalawat serta pengajian. Pada kesempatan ini pengajian\r\ndiisi oleh saudara Akhmad Asmui dari prodi PAI. Beliau menurturkan tentang\r\nsejarah Nabi Muhammad serta akhlak-akhlaknya yang patut menjadi uswatun\r\nkhasanah bagi generasi muda.&Acirc;&nbsp;</span></p>\n', 'K0hdrh', 0, '2017-12-09', '185', '', 0, 1, '14480012', 1, '2017-12-10 23:36:59', '2017-12-10 23:36:59', NULL),
(81, 'Kajian Islami PGMI to Celebrate Maulid Prophet of Muhammad SAW 1439 H', 'kajian-islami-pgmi-to-celebrate-maulid-prophet-of-muhammad-saw-1439-h', '<p class=\"MsoNormal\" style=\"line-height:200%\"><span lang=\"IN\" style=\"font-size:\r\n12.0pt;line-height:200%;font-family:\" times new roman>12th of rabiul awal is the day by moslems to celebrate a big\r\nday of the greatest prophet, habibana Muhammad saw. In this mounth many moslems\r\ncelebrate Maulid Muhammad with some benefit events, such as: pengajian akbar,\r\nreading al barzanji, shalawat together, etc.<p></p></span><p class=\"MsoNormal\" style=\"line-height:200%\"><span lang=\"IN\" style=\"font-size:\r\n12.0pt;line-height:200%;font-family:\" times new roman>KIP is one of events to celebrate maulid prophet muhammad\r\nsaw. KIP is council of reading al barzanji and shalawat which is presented by\r\nhadroh al ibtidaiyah PGMI. This event has been held within 3 years.<p></p></span></p><p class=\"MsoNormal\" style=\"line-height:200%\"><span lang=\"IN\" style=\"font-size:\r\n12.0pt;line-height:200%;font-family:\" times new roman>In this year, KIP has held on saturday 9th of december 2017/12th\r\nof rabiul awal 1439 H at arif mustofa\'s house who is one of hadroh al\r\nibtidaiyah members. The audience of KIP are from UIN Students, UGM, UST and\r\nmany more. KIP sponsored and presented by mr. Nur Hidayat as builder of hadroh\r\nal ibtidaiyah.<p></p></span></p><span lang=\"IN\" style=\"font-size:12.0pt;line-height:107%;font-family:\" times new roman mso-fareast-font-family: en-us>The point of KIP are reading al barzanji, shalawat,\r\nand speech. The content of speech presented by ahmad asmui from PAI. He told about\r\nhistory of prophet Muhammad and akhlak of Muhammad who should be role model for\r\nyoung generation.</span></p>\n', 'K0hdrh', 1, '2017-12-09', '185', '', 0, 1, '14480012', 1, '2017-12-10 23:36:59', '2017-12-10 23:36:59', NULL),
(82, 'KTP Dance dalam Acara Pelatihan Praktek Manajerial di Universitas Kebangsaan Malaysia', 'ktp-dance-dalam-acara-pelatihan-praktek-manajerial-di-universitas-kebangsaan-malaysia', '<div id=\":122\" class=\"ii gt adP adO\" style=\"font-size: 12.8px; direction: ltr; margin: 5px 15px 0px 0px; padding-bottom: 5px; position: relative; color: rgb(34, 34, 34); font-family: arial, sans-serif;\"><div id=\":is\" class=\"a3s aXjCH m1604123b2f29eb69\" style=\"overflow: hidden;\"><div dir=\"auto\"><div dir=\"auto\">Penampilan KTP Dance dalam acara Pelatihan Praktek Manajerial di Universitas Kebangsaan Malaysia dengan tarian Cublak-Cublak Suweng. Penampilan ini bertujuan untuk mengenalkan salah satu kebudayaan yang ada di Indonesia.</div><div><br></div></div><div class=\"yj6qo\"></div></div><div class=\"hq gt\" id=\":vl\" style=\"margin: 15px 0px; clear: both; font-size: 12.8px; color: rgb(34, 34, 34); font-family: arial, sans-serif;\"></div></div>\n', 'K0klta', 0, '2017-11-01', '186', '', 1, 1, '15480092', 1, '2017-12-10 23:41:14', '2017-12-11 00:15:05', NULL),
(83, 'KTP Dance in the Training of Managerial Practices at Universiti Kebangsaan Malaysia', 'ktp-dance-in-the-training-of-managerial-practices-at-universiti-kebangsaan-malaysia', '<p>Appearance of KTP Dance in Training of Managerial Practice at Universiti Kebangsaan Malaysia with Cublak-Cublak Suweng dance. This performance aims to introduce one of the existing culture in Indonesia.</p>\n', 'K0klta', 1, '2017-11-01', '186', '', 0, 1, '15480092', 1, '2017-12-10 23:41:14', '2017-12-10 23:41:14', NULL),
(84, 'KTP Dance dalam Acara Stadium General', 'ktp-dance-dalam-acara-stadium-general', '<span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Penampilan KTP Dance dengan Tari Nirmala dalam acara Stadium General dengan tema \"Mempersiapkan Calon Pendidik Profesional di Era Distrubutif\"</span>\n', 'K0klta', 0, '2017-10-05', '187', '', 0, 1, '15480092', 1, '2017-12-10 23:44:05', '2017-12-10 23:44:05', NULL),
(85, 'KTP Dance in Stadium General', 'ktp-dance-in-stadium-general', '<p>Appearance of KTP Dance with Nirmala Dance in Stadium General event with theme \"Preparing Prospective Educator in Distrubutif Professional Era\"</p>\n', 'K0klta', 1, '2017-10-05', '187', '', 0, 1, '15480092', 1, '2017-12-10 23:44:05', '2017-12-10 23:44:05', NULL),
(86, 'Penampilan KTP Dance dalam Acara Festival Bahasa dan Budaya yang Diselenggarakan oleh SPBA', 'penampilan-ktp-dance-dalam-acara-festival-bahasa-dan-budaya-yang-diselenggarakan-oleh-spba', '<div>Tari Cindai merupakan salah satu tari melayu moderen. Tari ini diiringi dengan lagu cindai yang populerkan oleh Siti Nurhaliza. Tari Cindai<div>menggambarkan seorang wanita bersolek dan berputus asa mengenang masa lalunya. Tarian ini sebenarnya suatu bentuk sastra lisan yang disampaikan secara berkelompok sambil menari. Selain itu, sekarang ini Tari Cindai juga merupakan tari muda mudi dengan gerakan yang kompak, dinamis, di mana mereka dituntun untuk selalu bekerja sama satu dengan yang lainnya.</div><div>Pegerakan Tari Cindai juga dilakukan dengan pegerakan sendiri atau inisiatif penari yang telah dipelajari dalam beberapa minggu, karena Tari Cindai</div><div>bukan salah satu dari tari tradisional adat mana pun</div><div>karena Tari Cindai bisa dimiliki dengan gerakan masing-masing yang kita inginkan.</div></div>\n', 'K0klta', 0, '2017-04-21', '188', '', 0, 1, '15480092', 1, '2017-12-10 23:48:51', '2017-12-10 23:48:51', NULL),
(87, 'Appearance of KTP Dance in Language and Culture Festival organized by SPBA', 'appearance-of-ktp-dance-in-language-and-culture-festival-organized-by-spba', '<div>Cindai dance is one of the modern Malay dance. This dance is accompanied by a cindai song popular by Siti Nurhaliza. Cindai Dance<div>describes a woman preening and despairing remembering her past. This dance is actually a form of oral literature that is delivered in groups while dancing. In addition, nowadays Cindai Dance is also a young dance of mudi with a compact, dynamic movement, where they are guided to always cooperate with one another.</div><div>The Cindai Dance Pegerakan is also done with its own pegel or dance initiative that has been studied in a few weeks, because of Cindai Dance</div><div>not one of any traditional traditional dance</div><div>because Cindai Dance can be owned with each movement we want.</div></div>\n', 'K0klta', 1, '2017-04-21', '188', '', 0, 1, '15480092', 1, '2017-12-10 23:48:51', '2017-12-10 23:48:51', NULL),
(88, 'Penampilan KTP Dance dalam Milad JQH Al-Mizan dengan Tari Cindai', 'penampilan-ktp-dance-dalam-milad-jqh-al-mizan-dengan-tari-cindai', '<div>Tari Cindai merupakan salah satu tari melayu moderen. Tari ini diiringi dengan lagu cindai yang populerkan oleh Siti Nurhaliza. Tari Cindai<div>menggambarkan seorang wanita bersolek dan berputus asa mengenang masa lalunya. Tarian ini sebenarnya suatu bentuk sastra lisan yang disampaikan secara berkelompok sambil menari. Selain itu, sekarang ini Tari Cindai juga merupakan tari muda mudi dengan gerakan yang kompak, dinamis, di mana mereka dituntun untuk selalu bekerja sama satu dengan yang lainnya.</div><div>Pegerakan Tari Cindai juga dilakukan dengan pegerakan sendiri atau inisiatif penari yang telah dipelajari dalam beberapa minggu, karena Tari Cindai</div><div>bukan salah satu dari tari tradisional adat mana pun</div><div>karena Tari Cindai bisa dimiliki dengan gerakan masing-masing yang kita inginkan.</div></div>\n', 'K0klta', 0, '2017-10-30', '189', '', 0, 1, '15480092', 1, '2017-12-10 23:52:32', '2017-12-10 23:52:32', NULL),
(89, 'Appearance of KTP Dance in Milad JQH Al-Mizan with Cindai Dance', 'appearance-of-ktp-dance-in-milad-jqh-al-mizan-with-cindai-dance', '<div>Cindai dance is one of the modern Malay dance. This dance is accompanied by a cindai song popular by Siti Nurhaliza. Cindai Dance<div>describes a woman preening and despairing remembering her past. This dance is actually a form of oral literature that is delivered in groups while dancing. In addition, nowadays Cindai Dance is also a young dance of mudi with a compact, dynamic movement, where they are guided to always cooperate with one another.</div><div>The Cindai Dance Pegerakan is also done with its own pegel or dance initiative that has been studied in a few weeks, because of Cindai Dance</div><div>not one of any traditional traditional dance</div><div>because Cindai Dance can be owned with each movement we want.</div></div>\n', 'K0klta', 1, '2017-10-30', '189', '', 0, 1, '15480092', 1, '2017-12-10 23:52:32', '2017-12-10 23:52:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_en` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `judul`, `judul_en`, `tanggal`, `jam`, `aktif`, `created_at`, `updated_at`) VALUES
(2, 'Workshop Mengajar', 'Workshop teaching', '2017-01-11', '00:00:00', 1, '2017-08-03 17:00:00', '2017-11-02 17:00:00'),
(3, 'Kunjungan Industri', 'visit the insdustry', '2017-05-01', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(4, 'Lomba Futsal PGMI', 'Race Futsal', '2017-06-08', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(6, 'Seminar Internasional', 'Seminar International', '2017-10-12', '00:00:00', 1, '2017-11-30 18:50:38', '2017-11-30 18:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id_gallery` bigint(20) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id_gallery`, `gambar`, `kategori`, `created_at`, `updated_at`, `deleted_at`) VALUES
(171, '1512943440.jpg', 'K0ppm', '2017-12-10 22:04:00', '2017-12-10 22:04:00', NULL),
(172, '5a2daf50c3612.jpeg', 'K0ppm', '2017-12-10 22:04:00', '2017-12-10 22:04:00', NULL),
(173, '5a2daf50dc93b.jpeg', 'K0ppm', '2017-12-10 22:04:00', '2017-12-10 22:04:00', NULL),
(174, '5a2daf50ed4f4.jpeg', 'K0ppm', '2017-12-10 22:04:00', '2017-12-10 22:04:00', NULL),
(175, '5a2daf5117729.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(176, '5a2daf5138942.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(177, '5a2daf5180396.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(178, '5a2daf5199163.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(179, '5a2daf51b6abb.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(180, '5a2daf51e3e45.jpeg', 'K0ppm', '2017-12-10 22:04:01', '2017-12-10 22:04:01', NULL),
(181, '5a2daf521a9e5.jpeg', 'K0ppm', '2017-12-10 22:04:02', '2017-12-10 22:04:02', NULL),
(182, '1512945213.jpg', 'K0klta', '2017-12-10 22:33:33', '2017-12-10 22:33:33', NULL),
(183, '1512947595.jpg', 'K0klta', '2017-12-10 23:13:15', '2017-12-10 23:13:15', NULL),
(184, '1512947814.jpg', 'K0klta', '2017-12-10 23:16:54', '2017-12-10 23:16:54', NULL),
(185, '1512949019.jpg', 'K0hdrh', '2017-12-10 23:36:59', '2017-12-10 23:36:59', NULL),
(186, '1512949274.jpg', 'K0klta', '2017-12-10 23:41:14', '2017-12-10 23:41:14', NULL),
(187, '1512949445.jpg', 'K0klta', '2017-12-10 23:44:05', '2017-12-10 23:44:05', NULL),
(188, '1512949731.jpg', 'K0klta', '2017-12-10 23:48:51', '2017-12-10 23:48:51', NULL),
(189, '1512949952.jpg', 'K0klta', '2017-12-10 23:52:32', '2017-12-10 23:52:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategories`
--

CREATE TABLE `kategories` (
  `id_kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaEn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategories`
--

INSERT INTO `kategories` (`id_kategori`, `nama`, `namaEn`, `gol`, `created_at`, `updated_at`, `deleted_at`) VALUES
('K0hdrh', 'KAJIAN', 'Study', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0hmps', 'HMPS', 'HMPS', '', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0klta', 'Kesenian', 'art', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0kuni', 'Kunjungan', 'Tour', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0mkrb', 'Keakraban', 'Familiarity', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0olga', 'Olahraga', 'Sport', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0ppm', 'PPM', 'PPM', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0prts', 'Prestasi', 'achievement', '', '2017-12-07 17:00:00', '2017-12-07 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_11_10_140832_create_makrabs_table', 2),
(13, '2017_11_10_142600_create_makrabs_table', 3),
(14, '2017_11_10_150422_create_blogs_table', 4),
(15, '2017_11_10_151312_create_kategories_table', 5),
(16, '2017_11_10_152429_create_galleries_table', 6),
(17, '2017_11_10_234910_create_events_table', 7),
(18, '2017_11_11_154858_create_navs_table', 8),
(19, '2017_12_04_004239_create_notifikations_table', 9),
(20, '2017_12_07_223458_create_table_prestasi', 10);

-- --------------------------------------------------------

--
-- Table structure for table `navs`
--

CREATE TABLE `navs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nav` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subnav` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navs`
--

INSERT INTO `navs` (`id`, `nav`, `subnav`, `bahasa`, `created_at`, `updated_at`) VALUES
(1, 'BERANDA', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(2, 'HMPS', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(3, 'HOME', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(4, 'HMPS', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(5, 'KEGIATAN', '', 0, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(6, 'ACTIVITIES', '', 1, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(7, 'CARA UPLOAD', '', 0, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(8, 'GUIDE UPLOAD', '', 1, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(9, 'EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(10, 'EKSTRA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(11, 'TOUR', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(12, 'KUNJUNGAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(13, 'SPORT', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(14, 'OLAHRAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(15, 'KESENIAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(16, 'ART', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(17, 'GALERI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(18, 'GALLERY', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(19, 'Cari', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(20, 'Search', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(21, 'KEGIATAN MAHASISWA PGMI UIN SUNAN KALIJAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(22, 'ACTIVITY STUDENT PGMI UIN SUNAN KALIJAGA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(23, 'POPULAR POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(24, 'POST POPULER', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(25, 'Selengkapnya', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(26, 'Read More', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(27, 'EKSTRA MAHASISWA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(28, 'STUDENT EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(29, 'Kategori', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(30, 'Category', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(31, 'POST TERBARU', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(32, 'NEW POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(33, 'Date', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(34, 'Tanggal', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(35, 'Created By', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(36, 'Dibuat Oleh', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(37, 'AGENDA TERKINI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(38, 'NEW EVENTS', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(39, 'MOST CONTRIBUTE', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(40, 'KONTRIBUTOR TERAKTIF', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(41, 'PRESTASI', '', 0, NULL, NULL),
(42, 'ACHIEVEMENT', '', 1, NULL, NULL),
(43, 'KEAKRABAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(44, 'FAMILIARITY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(45, 'KAJIAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(46, 'STUDY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(47, 'POST TERKAIT', '', 0, '2017-12-15 17:00:00', '2017-12-15 17:00:00'),
(48, 'RELATED POST', '', 1, '2017-12-09 17:00:00', '2017-12-09 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notifikations`
--

CREATE TABLE `notifikations` (
  `id` int(10) UNSIGNED NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_content` int(11) NOT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peserta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sifat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bhs` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgDesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `id_gambar` int(11) NOT NULL,
  `captionId` text NOT NULL,
  `captionEn` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `id_gambar`, `captionId`, `captionEn`, `created_at`, `updated_at`) VALUES
(2, 52, 'Caption2Idhe', 'Caption2En', '2017-11-26 17:00:00', '2017-12-06 06:44:52'),
(3, 53, 'Caption3Id', 'Caption3En', '2017-11-27 17:00:00', '2017-12-06 06:51:41'),
(4, 51, 'Seminar Nasional', 'Seminar National', '2017-11-30 20:00:49', '2017-11-30 20:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sekar', 'sekar@pgmi', 'sekarpgmi', '$2y$10$iXAPIrZykXz8LEqXD9GUAOxSZDWcGlsZ7l97lVIPu.m231R.Svyza', 'OlXrpVkgyYQAwcPMBH3IcmTr2CUmE41gYrBMf3v5Orv0tRUyOfVX5eDZaTUo', '2017-12-08 22:08:29', '2017-12-08 22:08:29'),
(2, 'ayyub', 'ayyub@pgmi', 'ayyubpgmi', '$2y$10$OiApDU4/06NLowtIwL9Uj.UMTh1RDjlYhcEOIU9bVsUPpKafJjNtC', 'QQBNijOyC5l4s7QjF8L4FpGq2lskdUsVnZkHecCz2nSRUBdBJq5sD7B6gycN', '2017-12-08 22:24:57', '2017-12-08 22:24:57'),
(3, 'pgmi', 'superpgmi', 'superpgmi', '$2y$10$EvWhF2qJftsPMaBB0jbWSOCXAtj9pUrXpub3TSwKvdBBllIDwr4HK', 'xP2KmmAkzPJsQ82uswzRIkctHzt9D25JOFPOv2pLCw4DukLHKXKHSVeeFfsL', '2017-12-08 23:45:48', '2017-12-08 23:45:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `kategories`
--
ALTER TABLE `kategories`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navs`
--
ALTER TABLE `navs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikations`
--
ALTER TABLE `notifikations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id_gallery` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `navs`
--
ALTER TABLE `navs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `notifikations`
--
ALTER TABLE `notifikations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
