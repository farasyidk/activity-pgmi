-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 10, 2017 at 10:31 PM
-- Server version: 5.7.20-0ubuntu0.17.10.1
-- PHP Version: 7.1.11-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_pgmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `img` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgContent` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` bigint(20) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `judul`, `slug`, `content`, `kategori`, `bahasa`, `tanggal`, `img`, `imgContent`, `view`, `aktif`, `from`, `baca`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'FITK UIN Sunan Kalijaga Resmi Lepas Delegasi Mahasiswa ke Malaysia', 'FITK-UIN-Sunan-Kalijaga-Resmi.html', 'Fakultas Ilmu Tarbiyah dan Keguruan (FITK) resmi lepas peserta Praktik Pelatihan Manajerial (PPM) dan Student Exchange (SE) di ruang pertemuan FITK pada Rabu 25 Oktober silam. Prodi Pendidikan Guru Madrasah Ibtidaiyah (PGMI) juga turut serta mengirimkan mahasiswanya untuk belajar singkat selama empat hari di Negeri Jiran tersebut. Terhitung dari tanggal 30 Oktober – 2 November, 66 mahasiswa yang terdiri dari 46 peserta PPM (dari prodi PGMI) dan 20 peserta SE (dari perwakilan semua jurusan di fakultas) di atas akan diagendakan untuk berkunjung ke sejumlah universitas di Malaysia, yakni University of Malaya (UM), Institut Pendidikan Guru Kampus Bahasa Melayu (IPGKBM), dan Universiti Kebangsaan Malaysia (UKM). Selaku penanggungjawab program kunjungan ke Malaysia ini, Bu Fitri Yuliawati menjelaskan bahwa pada masing-masing universitas, telah menyetujui dan menerima kunjungan dari UIN Sunan Kalijaga dengan mengirimkan Letter of Acceptance lengkap dengan lokasi penyambutan. Mahasiswa akan diterima Fakulti Education UM pada tanggal 31 Oktober di Cemerlang Meeting Room oleh dekan Prof. Dr. Rohaida Mohd Saat. Tanggal 1 November, penyambutan UKM dilaksanakan di Gerbang Minda Fakulti Pendidikan oleh Dekan YBhg. Prof. Dato’ dan Dr. Norazah Mohd Nordin. Untuk selanjutnya ialah kunjungan ke IPGKBM yang berlokasi di Lembah Pantai, Kuala Lumpur oleh Nurulhana Hussain. PPM selaku program yang diusung oleh prodi PGMI mengangkat tema “Harmoni Kebudayaan dalam Semangat Kebersamaan Menuju Pendidikan Bertaraf Internasional” dalam kesempatan kali ini. Tujuan kunjungan ini berdasarkan keterangan Bu Endang Sulistyowati, salah satu dosen PGMI yang turut serta, ialah studi banding untuk melihat proses pembelajaran di Departemen Pendidikan masing-masing universitas. Kegiatan PPM ini lebih menekankan pada observasi dalam pembelajaran. Tetapi tidak hanya itu saja. Bu Endang, sapaan akrabnya, mengatakan bahwa ada beberapa alasan mengapa kegiatan PPM ke luar negeri seperti ini sangat penting untuk dilaksanakan. Pertama, mahasiswa akan memiliki pengalaman mengurus segala sesuatu ke luar negeri (entah itu paspor, visa, dan lain-lain). Kedua, adanya kunjungan ini agar dapat memacu motivasi mahasiswa mencari beasiswa ke luar negeri. Ketiga, agar mahasiswa dapat melihat budaya Negara Malaysia beserta cara pembelajarannya. Termasuk saat berada di UM, sebagai salah satu kampus terbaik Negara Malaysia, mahasiswa dapat belajar dan mengetahui bagaimana kualifikasi dosen, mahasiswa, staf, sarana prasarana, lingkungan, dan lain sebagainya di sana. Tidak lain kegiatan ini juga dilatarbelakangi oleh kepentingan prodi PGMI di tahun depan yang akan mengajukan akreditasi ASEAN (AUN-QA), yang salah satu indikatornya kegiatan mahasiswa harus berorientasi ke luar negeri. Setiap tahunnya, PPM ini selalu diselenggarakan oleh prodi PGMI. Kegiatan kemahasiswaan ini merupakan kegiatan yang hampir sepenuhnya diakomodir oleh mahasiswa sendiri, tentunya menggunakan uang pribadi untuk berangkat ke lokasi tujuan, termasuk ke Malaysia. Sebelumnya pada PPM pertama, tujuan kunjungan ialah ke SD Semesta Semarang dan di Prodi PGSD UNNES Semarang. Kedua, berlokasi di SD Brawijaya Malang dan PGMI UIN Malang. Ini merupakan kali ketiga program PPM terlaksana. (Arina)', 'K0hmps', 0, NULL, '1', '0', 36, 1, '16650021', 1, '2017-11-10 17:00:00', '2017-12-08 16:14:51', '2017-12-08 16:14:51'),
(2, '[english]FITK UIN Sunan Kalijaga Resmi Lepas Delegasi Mahasiswa ke Malaysia', '[english]FITK-UIN-Sunan-Kalijaga-Resmi', 'Beasiswa sure to hal yang didambakan bagi para mahasiswa dalam menempuh kuliah. Selain meringankan beban perekonomian orang tua, juga mampu membuat seseorang terpacu untuk meningkatkan prestasi blalalallalalalallalaalalallalalalalala\r\n<p>isi baksos indo<img data-filename=\"PGMI.jpg\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a28b9ec0c6f1.jpeg\"><blockquote><p><span style=\"font-size: 36px;\"><b><i><u><br></u></i></b></span></p><hr><p><span style=\"font-size: 36px;\"><b><i><u>&nbsp;</u></i></b></span><a href=\"http://facebook.com\" target=\"_blank\">facebook</a></p></blockquote><p><img data-filename=\"PGMI.jpg\" style=\"width: 285.5px;\" src=\"http://localhost:8000/assets/img/blogs/5a28b9ec24f4d.jpeg\"></p><p><br></p><hr><p><br></p></p>\r\n', 'K0hmps', 1, NULL, '1', '0', 10, 1, '16650021', 1, '2017-11-11 17:00:00', '2017-12-08 16:15:10', '2017-12-08 16:15:10'),
(3, 'Prodi BKI Gelar International Forum Discussion', 'Prodi-BKI-Gelar-International-Forum', '[indo]Program Studi Bimbingan dan Konseling Islam UIN Sunan Kalijaga kembali menjalin kerjasama internasional dengan Universiti Sains Islam Malaysia (USIM). Kali ini sebanyak 30 orang mahasiswa Program Konsentrasi', 'K0ppm', 0, NULL, '2', '0', 16, 1, '16650021', 1, '2017-11-11 17:00:00', '2017-12-09 05:57:53', NULL),
(4, '[english]Prodi BKI Gelar International Forum Discussion', '[english]Prodi-BKI-Gelar-International', '[english]Program Studi Bimbingan dan Konseling Islam UIN Sunan Kalijaga kembali menjalin kerjasama internasional dengan Universiti Sains Islam Malaysia (USIM). Kali ini sebanyak 30 orang mahasiswa Program Konsentrasi', 'K0ppm', 1, NULL, '2', '0', 6, 1, '16650021', 1, '2017-11-11 17:00:00', '2017-12-09 06:39:47', NULL),
(5, 'Pameran Media dan Alat Pembelajaran', 'Pameran-Media-dan-Alat-Pembelajaran', 'Pameran media pembelajaran dan praktik edupreneurship PGMI terlaksana pada Sabtu, 18 September 2017 di Convention Hall UIN Sunan Kalijaga', 'K0kuni', 0, NULL, '3', '0', 31, 1, '16650030', 1, '2017-11-09 17:00:00', '2017-12-09 07:28:02', NULL),
(6, '[english]Pameran Media dan Alat Pembelajaran', '[english]Pameran-Media-dan-Alat', '[english]Pameran media pembelajaran dan praktik edupreneurship PGMI terlaksana pada Sabtu, 18 September 2017 di Convention Hall UIN Sunan Kalijaga', 'K0kuni', 1, NULL, '3', '0', 17, 1, '16650030', 1, '2017-11-11 17:00:00', '2017-12-09 08:07:32', NULL),
(7, 'Makrab 2017 up', 'makrab-2017-up', '<p>                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<p><img style=\"width: 50%;\" data-filename=\"Screenshot_2017-12-03_09-28-44.png\" src=\"http://localhost:8000/assets/img/blogs/5a2b230155e2a.png\"><br></p><p>\r\n                                </p></p>\n', '0', 0, NULL, '1', '154', 6, 1, '16650020', 1, '2017-11-11 17:00:00', '2017-12-08 16:40:49', NULL),
(8, 'Kunjungan Industri', 'Kunjungan-Industri', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'K0hdrh', 0, NULL, '2', '0', 13, 1, '16650021', 1, '2017-11-11 17:00:00', '2017-12-09 06:12:58', NULL),
(9, 'ini judul', 'ini-judul', '<p>ini content<img src=\"assets/img/blogs/nama.jpg\"></p>\n', 'K0hmps', 0, NULL, '6', '0', 5, 1, '16650063', 0, '2017-11-26 07:29:33', '2017-12-03 16:57:19', '2017-12-03 16:57:19'),
(10, 'this is judul edit', 'this-is-judul-edit', '<blockquote>ini content<img src=\"assets/img/blogs/jeneng.jpg\">ku haha<p>Lorem ipsum, bla blaaa mantap</p><p><img style=\"width: 50%;\" data-filename=\"tanya3.png\" src=\"http://localhost:8000/assets/img/blogs/5a2b26ff063e3.png\"><br></p><p></p><p></p><p></p><p></p></blockquote>\n', 'K0hmps', 1, NULL, '6', '', 1, 2, '16650063', 0, '2017-11-26 07:29:33', '2017-12-09 07:01:48', '2017-12-09 07:01:48'),
(11, 'coba post judul olahragah', 'coba-post-judul-olahragah', '<p>coba post content olahraga</p>\n', 'K0olga', 0, NULL, '36', '0', 0, 0, '16650063', 0, '2017-11-26 11:37:08', '2017-12-01 08:59:49', '2017-12-01 08:59:49'),
(12, 'test post judul olahraga', 'test-post-judul-olahraga', '<p>test post content olahraga</p>\n', 'K0olga', 1, NULL, '7', '0', 0, 1, '16650063', 0, '2017-11-26 11:37:08', '2017-12-01 09:01:36', '2017-12-01 09:01:36'),
(13, 'pelantikan ayub menjadi CEO JustFriend week', 'pelantikan-ayub-menjadi-ceo-justfriend-week', '<p>[indo]pelantikan ayub menjadi CEO JustFriend                                  \r\n                               </p>\n', 'K0hmps', 0, NULL, '8', '0', 0, 1, '16650063', 1, '2017-11-29 14:55:28', '2017-12-08 16:13:10', '2017-12-08 16:13:10'),
(14, '[en]pelantikan ayub menjadi CEO JustFriend', 'enpelantikan-ayub-menjadi-ceo-justfriend', '<p>[English]pelantikan ayub menjadi CEO JustFriend</p>\n', 'K0hmps', 1, NULL, '8', '0', 2, 1, '16650063', 1, '2017-11-29 14:55:28', '2017-12-09 15:38:43', NULL),
(15, 'Pelantikan Presiden Ayub Alfarabi', 'pelantikan-presiden-ayub-alfarabi', '<p>[id]Pelantikan Presiden Ayub Alfarabi</p>\n', 'K0olga', 0, NULL, '1', '0', 0, 1, '16650063', 1, '2017-11-30 19:02:45', '2017-12-08 16:17:09', '2017-12-08 16:17:09'),
(16, '[en]Pelantikan Presiden Ayub Alfarabi', 'enpelantikan-presiden-ayub-alfarabi', '<p>[english]Pelantikan Presiden Ayub Alfarabi</p>\n', 'K0olga', 1, NULL, '37', '0', 4, 1, '16650063', 1, '2017-11-30 19:02:45', '2017-12-09 07:01:37', '2017-12-09 07:01:37'),
(17, 'mencoba judul indo', 'mencoba-judul-indo', '<p>&nbsp;ini content<p><img data-filename=\"Screenshot_2017-12-02_06-34-15.png\" style=\"width: 412.102px;\" src=\"assets/img/blogs/5a235047a49ed.png\"></p><p>                                  \r\n                               </p></p>\r\n', 'K0ppm', 0, NULL, '39', '0', 1, 1, '16650063', 1, '2017-12-02 18:15:51', '2017-12-09 06:00:21', NULL),
(18, 'mencoba judul English', 'mencoba-judul-english', '<p>ini content english</p>\n', 'K0ppm', 1, NULL, '39', '0', 0, 1, '16650063', 1, '2017-12-02 18:15:51', '2017-12-06 05:30:46', NULL),
(19, 'ini judul indo2', 'ini-judul-indo2', '<p>ini content kedua<p><img data-filename=\"singleBlade.png\" style=\"width: 412.102px;\" src=\"%7B%7Basset(\'http://localhost:8000/assets/img/blogs/5a23573005442.png\')%7D%7D\"></p><p>                                  \n                               </p></p>\n', 'K0kuni', 0, NULL, '40', '0', 0, 1, '16650063', 1, '2017-12-02 18:45:20', '2017-12-06 05:30:46', NULL),
(20, 'ini judul english2', 'ini-judul-english2', '<p>ini content kedua<img data-filename=\"tanya3.png\" style=\"width: 412.102px;\" src=\"%7B%7Basset(\'http://localhost:8000/assets/img/blogs/5a2357303b88a.png\')%7D%7D\"></p>\n', 'K0kuni', 1, NULL, '40', '0', 2, 1, '16650063', 1, '2017-12-02 18:45:20', '2017-12-09 08:02:01', NULL),
(21, 'informasi perkuliahan UIN2', 'informasi-perkuliahan-uin2', '<p>informasi<img style=\"width: 412.102px;\" data-filename=\"tanya1.png\" src=\"localhost:8000/http://localhost:8000/assets/img/blogs/5a235c8a4dcc4.png\"></p>\n', 'K0hmps', 0, NULL, '43', '0', 0, 1, '16650063', 1, '2017-12-02 19:08:10', '2017-12-06 05:30:46', NULL),
(22, 'informasi perkuliahan UINeN2', 'informasi-perkuliahan-uinen2', '<p>Information<img data-filename=\"tanya1.png\" style=\"width: 412.102px;\" src=\"localhost:8000/http://localhost:8000/assets/img/blogs/5a235c8a60b78.png\"></p>\n', 'K0hmps', 1, NULL, '43', '0', 0, 1, '16650063', 1, '2017-12-02 19:08:10', '2017-12-06 05:30:46', NULL),
(23, 'informasi perkuliahan UIN3', 'informasi-perkuliahan-uin3', '<p>informasi3<p>hehe mantap</p></p>\n', '0', 0, NULL, '44', '', 4, 1, '16650063', 1, '2017-12-02 19:10:52', '2017-12-08 16:39:52', NULL),
(24, 'informasi perkuliahan UINeN3', 'informasi-perkuliahan-uinen3', '<p>Information3<img data-filename=\"tbo1.png\" style=\"width: 412.102px;\" src=\"http://localhost:8000/assets/img/blogs/5a235d2c93bf6.png\">                                  \r\n                               </p>\n', 'K0kuni', 1, NULL, '44', '0', 1, 1, '16650063', 1, '2017-12-02 19:10:52', '2017-12-08 16:32:10', '2017-12-08 16:32:10'),
(25, 'judul Post dari ayyub', 'judul-post-dari-ayyub', '<p>ini contentku, ayyay&nbsp;&nbsp;&nbsp;&nbsp;</p>\n', 'K0mkrb', 0, NULL, '45', '0', 0, 1, '16650063', 1, '2017-12-03 15:21:38', '2017-12-08 15:38:25', '2017-12-08 15:38:25'),
(26, 'title post from ayyub', 'title-post-from-ayyub', '<p>this is my content</p>\n', 'K0mkrb', 1, NULL, '45', '0', 0, 1, '16650063', 1, '2017-12-03 15:21:38', '2017-12-08 15:44:46', '2017-12-08 15:44:46'),
(27, 'judul Post dari ayyub2', 'judul-post-dari-ayyub2', '<p>ini contentku, ayyay&nbsp; &nbsp; 2</p>\n', 'K0klta', 0, NULL, '46', '0', 0, 1, '16650063', 1, '2017-12-03 15:25:28', '2017-12-08 15:41:24', '2017-12-08 15:41:24'),
(28, 'title post from ayyub2', 'title-post-from-ayyub2', '<p>this is my content2</p>\n', 'K0klta', 1, NULL, '2', '0', 4, 1, '16650063', 1, '2017-12-03 15:25:28', '2017-12-08 16:30:51', '2017-12-08 16:30:51'),
(29, 'Seminar Nasional Ayyub', 'seminar-nasional-ayyub', '<p>Sminar Nasio dari pak Ayyub</p>\n', 'K0hdrh', 0, NULL, '47', '0', 0, 0, '16650063', 0, '2017-12-03 15:26:58', '2017-12-03 17:05:18', '2017-12-03 17:05:18'),
(30, 'Seminar National Ayyub', 'seminar-national-ayyub', '<p>Sminar Nasio from pak Ayyub</p>\n', 'K0hdrh', 1, NULL, '47', '0', 0, 0, '16650063', 0, '2017-12-03 15:26:58', '2017-12-03 17:06:15', '2017-12-03 17:06:15'),
(31, 'Seminar Nasional pak ayyub2 haha', 'seminar-nasional-pak-ayyub2-haha', '<p>seminar dari pak ayyub2 hehe</p>\n', 'K0kuni', 0, NULL, '48', '', 0, 1, '16650063', 1, '2017-12-03 15:32:39', '2017-12-08 18:08:55', NULL),
(32, 'Seminar National pak ayyub2', 'seminar-national-pak-ayyub2', '<p>Seminar from pak ayyub2</p>\n', 'K0kuni', 1, NULL, '48', '0', 0, 0, '16650063', 1, '2017-12-03 15:32:39', '2017-12-06 05:30:46', NULL),
(33, 'Seminar Nasional pak ayyub2', 'seminar-nasional-pak-ayyub2', '<p>seminar dari pak ayyub2</p>\n', 'K0kuni', 0, NULL, '49', '0', 0, 0, '16650063', 1, '2017-12-03 15:34:46', '2017-12-06 05:30:46', NULL),
(34, 'Seminar National pak ayyub2', 'seminar-national-pak-ayyub2', '<p>Seminar from pak ayyub2</p>\n', 'K0kuni', 1, NULL, '49', '0', 0, 0, '16650063', 0, '2017-12-03 15:34:46', '2017-12-03 17:12:45', '2017-12-03 17:12:45'),
(35, 'Outbond Akbar PGMI2 ayyub', 'outbond-akbar-pgmi2-ayyub', '<p>outbond besar bersama pak ayyub</p>\n', 'K0kuni', 0, NULL, '50', '0', 0, 0, '16650063', 1, '2017-12-03 15:45:29', '2017-12-06 05:30:46', NULL),
(36, 'Big Outbond PGMI2 ayyub', 'big-outbond-pgmi2-ayyub', '<p>big outbond with mr ayyub</p>\n', 'K0kuni', 1, NULL, '50', '0', 0, 0, '16650063', 1, '2017-12-03 15:45:29', '2017-12-06 05:30:46', NULL),
(37, 'TESTER web indone', 'tester-web-indone', '<p style=\'margin-bottom: 20px; color: rgb(119, 119, 119); line-height: 24px; font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Pertengahan tahun 2018 di Prodi PGMI akan dilakukan visitasi untuk&nbsp;</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\"><b>akreditasi</b> tingkat Asean, yaitu AUQ-QA. Sebagai Prodi yang</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">terakreditasi secara Internasional, Prodi PGMI mendorong mahasiswa</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">agar melakukan kegiatan-kegiatan yang berlevel Internasional, walau</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">masih sebatas negara-negara di ASEAN.</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Pada tanggal 31 Oktober 2017, sebanyak 44 mahasiswa PGMI semester V</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">melakukan kunjungan akademik ke University of Malaya, dalam rangka kegiatan PPM (Pelatihan Praktik Manjerial).Bersamaan dengan kegiatan PPM ini adalah kegiatan&Atilde;&#131;&AElig;&#146;&Atilde;&#134;&acirc;&#128;&#153;&Atilde;&#131;&Acirc;&cent;&Atilde;&cent;&acirc;&#128;&#154;&Acirc;&not;&Atilde;&#133;&Acirc;&iexcl;&Atilde;&#131;&AElig;&#146;&Atilde;&cent;&acirc;&#130;&not;&Aring;&iexcl;&Atilde;&#131;&acirc;&#128;&#154;&Atilde;&#130;&nbsp;<em>Student Exchange</em>&Atilde;&#131;&AElig;&#146;&Atilde;&#134;&acirc;&#128;&#153;&Atilde;&#131;&Acirc;&cent;&Atilde;&cent;&acirc;&#128;&#154;&Acirc;&not;&Atilde;&#133;&Acirc;&iexcl;&Atilde;&#131;&AElig;&#146;&Atilde;&cent;&acirc;&#130;&not;&Aring;&iexcl;&Atilde;&#131;&acirc;&#128;&#154;&Atilde;&#130;&nbsp;yang</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">diikuti oleh 20 mahasiswa Fakultas Ilmu Tarbiyah dan Keguruan (FITK)</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">UIN Sunan Kalijaga, yang terdiri dari 5 Jurusan/Prodi, yaitu: PAI, MPI,</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">PBA, PGMI, dan PIAUD. Dalam kunjungan ini rombongan diterima oleh Prof. Dr. Zaharah Hussin</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">mewakili dekan&Atilde;&#131;&AElig;&#146;&Atilde;&#134;&acirc;&#128;&#153;&Atilde;&#131;&Acirc;&cent;&Atilde;&cent;&acirc;&#128;&#154;&Acirc;&not;&Atilde;&#133;&Acirc;&iexcl;&Atilde;&#131;&AElig;&#146;&Atilde;&cent;&acirc;&#130;&not;&Aring;&iexcl;&Atilde;&#131;&acirc;&#128;&#154;&Atilde;&#130;&nbsp;<em>Faculty of Education</em>&Atilde;&#131;&AElig;&#146;&Atilde;&#134;&acirc;&#128;&#153;&Atilde;&#131;&Acirc;&cent;&Atilde;&cent;&acirc;&#128;&#154;&Acirc;&not;&Atilde;&#133;&Acirc;&iexcl;&Atilde;&#131;&AElig;&#146;&Atilde;&cent;&acirc;&#130;&not;&Aring;&iexcl;&Atilde;&#131;&acirc;&#128;&#154;&Atilde;&#130;&nbsp;dan beberapa dosen lain.</span><p style=\'margin-bottom: 20px; color: rgb(119, 119, 119); line-height: 24px; font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Pada</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">kesempatan ini Prof. Dr. Zaharah menyampaikan profil dan keunggulan</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Fakultas Pendidikan di UM, dan diadakan sesi tanya jawab. Sesi yang</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">ditunggu-tunggu setelah sesi tanya jawab adalah penampilan mahasiswa.</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Pada kesempatan ini Prodi PGMI menampilkan paduan suara dengan</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">menyanyikan Tanah Airku dan Manuk Dadali. Ternyata lagu Manuk Dadali</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">yang berbahasa Sunda sangat menarik perhatian pihak UM, karena bahasa</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Sunda belum banyak dikenal.</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">Setelah berbagai acara, acara terakhir adalah bertukar cinderamata dan</span><span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;\">berfoto bersama.</span></p><p></p></p>\n', 'K0kuni', 0, NULL, '55', '0', 0, 0, '16650063', 1, '2017-12-06 07:53:45', '2017-12-06 08:09:15', '2017-12-06 08:09:15'),
(38, 'TESTER WEB', 'tester-web', '<p style=\'margin-bottom: 20px; color: rgb(119, 119, 119); line-height: 24px; font-family: \"Open Sans\", Arial, sans-serif; text-align: justify;\'><br class=\"Apple-interchange-newline\"><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; text-align: left; white-space: pre-wrap;\">Mid-year 2018 in Prodi PGMI will be conducted visitation for Asean-level accreditation, that is AUQ-QA. The internationally accredited Prodi, Prodi PGMI encourages students to conduct activities of international level, even as countries in ASEAN. On October 31, 2017, as many as 44 students of the PGMI semester conducted a visit to the University of Malaya, within the framework of PPM activities (Practice Training Manjerial). Together with this PPM activity is Student Exchange activity which is followed by 20 students of Faculty of Science Tarbiyah and Teacher Training (FITK) UIN Sunan Kalijaga, which consists of 5 Departments / Prodi, namely: PAI, MPI, PBA, PGMI, and PIAUD. In this visit the group received by Prof. Dr. Zaharah Hussin represents the dean of the Faculty of Education and several other lecturers.\r\n\r\nOn this occasion Prof. Dr. Zaharah conveyed the profile and usefulness of Faculty of Education at UM, and held question and answer session. Sessions that are awaited after the question and answer session is a student performance. On this occasion Prodi PGMI featuring a choir by singing my homeland and Manuk Dadali. It turns out that the song of Manuk Dadaliyang speaks Sunda very attract the attention of the UM, because the languageSunda not yet widely known.After the various events, the last event is exchanging souvenirs and taking pictures together.</span><br></p>\n', 'K0kuni', 1, NULL, '55', '0', 0, 0, '16650063', 1, '2017-12-06 07:53:45', '2017-12-06 08:09:32', '2017-12-06 08:09:32'),
(39, 'Seminar Internasional 3', 'seminar-internasional-3', '<p>isi dari seminar 3</p>\n', 'K0kuni', 0, NULL, '56', '0', 0, 1, '16650063', 1, '2017-12-06 20:28:35', '2017-12-08 15:45:45', '2017-12-08 15:45:45'),
(40, 'Seminar International 3', 'seminar-international-3', '<p>this is content 3</p>\n', 'K0kuni', 1, NULL, '56', '0', 1, 1, '16650063', 1, '2017-12-06 20:28:35', '2017-12-08 16:09:08', '2017-12-08 16:09:08'),
(41, 'Baksos Bahasa Indo', 'baksos-bahasa-indo', '<p>isi baksos indo<img data-filename=\"PGMI.jpg\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a28b9ec0c6f1.jpeg\"></p></p>\r\n', 'K0olga', 0, NULL, '57', '0', 3, 1, '16650063', 1, '2017-12-06 20:47:56', '2017-12-08 15:42:14', '2017-12-08 15:42:14'),
(42, 'Baksos Bahasa Inggris', 'baksos-bahasa-inggris', '<p>isi baksos inggris</p>\n', 'K0olga', 1, NULL, '57', '0', 1, 1, '16650063', 1, '2017-12-06 20:47:56', '2017-12-08 15:37:07', '2017-12-08 15:37:07'),
(43, 'Kegiatan Luar Kelas', 'kegiatan-luar-kelas', '<p>Kegiatan luar kelas dilakukan pada&nbsp; mata kuliah Kesenian dan Olahraga yang diberikan kepada mahasiswa semester 3.&nbsp;</p>\n', 'K0olga', 0, NULL, '58', '0', 0, 0, '16650063', 1, '2017-12-06 21:22:55', '2017-12-07 07:23:07', NULL),
(44, 'Other Activity Class', 'other-activity-class', '<p>Other activity class</p>\n', 'K0olga', 1, NULL, '58', '0', 0, 0, '16650063', 1, '2017-12-06 21:22:56', '2017-12-07 07:23:07', NULL),
(45, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hddhd<p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-32-13.png\" src=\"http://localhost:8000/assets/img/blogs/5a2954588bade.png\"></p><p>hahahah</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295458bbfdc.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '59', '0', 4, 0, '16650063', 1, '2017-12-07 07:46:49', '2017-12-09 07:30:58', NULL),
(46, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hddhd<p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-32-13.png\" src=\"http://localhost:8000/assets/img/blogs/5a2954d3975e3.png\"></p><p>hahahah</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a2954d3c5b4d.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '62', '0', 4, 0, '16650063', 1, '2017-12-07 07:48:52', '2017-12-09 07:30:58', NULL),
(47, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hddhd<p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-32-13.png\" src=\"http://localhost:8000/assets/img/blogs/5a2955868f24f.png\"></p><p>hahahah</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295586bd552.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '65', '0', 4, 0, '16650063', 1, '2017-12-07 07:51:51', '2017-12-09 07:30:58', NULL),
(48, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hddhd<p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-32-13.png\" src=\"http://localhost:8000/assets/img/blogs/5a2955a63cb60.png\"></p><p>hahahah</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a2955a66c734.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '68', '0', 4, 0, '16650063', 1, '2017-12-07 07:52:23', '2017-12-09 07:30:58', NULL),
(49, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hddhd<p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-32-13.png\" src=\"http://localhost:8000/assets/img/blogs/5a29563548c1f.png\"></p><p>hahahah</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295635768e1.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '71', '0', 4, 0, '16650063', 1, '2017-12-07 07:54:46', '2017-12-09 07:30:58', NULL),
(50, 'Seminar National2', 'seminar-national2', '<p>hahahaha</p>\n', 'K0mkrb', 1, NULL, '71', '0', 5, 1, '16650063', 1, '2017-12-07 07:54:46', '2017-12-09 15:46:09', NULL),
(51, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>sjshsds<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295703c1f63.png\"></p><p>gggg</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295703f04e4.png\"></p></p>\n', 'K0olga', 0, NULL, '74', '0', 4, 0, '16650063', 1, '2017-12-07 07:58:12', '2017-12-09 07:30:58', NULL),
(52, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhdhdhdh<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295d725a2b0.png\"></p><p>ddd</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295d7287a32.png\"><br></p></p>\n', 'K0olga', 0, NULL, '80', '0', 1, 0, '16650063', 1, '2017-12-07 08:25:39', '2017-12-08 19:07:47', '2017-12-08 19:07:47'),
(53, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhdhdhdh<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295de318d3c.png\"></p><p>ddd</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295de3475fe.png\"><br></p></p>\n', 'K0olga', 0, NULL, '83', '0', 1, 0, '16650063', 1, '2017-12-07 08:27:31', '2017-12-08 19:07:26', '2017-12-08 19:07:26'),
(54, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhdhdhdh mantap hehe<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295e398e08a.png\"></p><p>ddd</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295e39bb0d7.png\"><br></p><p></p><p></p><p></p></p>\n', 'K0olga', 0, NULL, '86', '', 4, 1, '16650063', 1, '2017-12-07 08:28:58', '2017-12-09 07:30:58', NULL),
(55, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhdhdhdh<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295eb10d6df.png\"></p><p>ddd</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295eb13b328.png\"><br></p></p>\n', 'K0olga', 0, NULL, '89', '90,91', 1, 0, '16650063', 1, '2017-12-07 08:30:57', '2017-12-08 17:19:52', '2017-12-08 17:19:52'),
(56, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhdhdhdh<p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295ecae4d63.png\"></p><p>ddd</p><p><img style=\"width: 25%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a295ecb1d5db.png\"><br></p></p>\n', 'K0olga', 0, NULL, '92', '93,94', 1, 0, '16650063', 1, '2017-12-07 08:31:23', '2017-12-08 16:05:59', '2017-12-08 16:05:59'),
(57, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>hhhh<img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295fdf336f4.png\"><p>ssssss</p><p><img data-filename=\"errorr.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a295fdf5f57b.png\"><br></p></p>\n', 'K0mkrb', 0, NULL, '95', '96,97', 1, 1, '16650063', 1, '2017-12-07 08:35:59', '2017-12-08 15:34:11', '2017-12-08 15:34:11'),
(58, 'jhhhhhhh', 'jhhhhhhh', '<p>ssseeheheheh</p>\n', 'K0mkrb', 1, NULL, '95,', '0', 0, 0, '16650063', 1, '2017-12-07 08:35:59', '2017-12-08 14:59:02', '2017-12-08 14:59:02'),
(59, 'informasi perkuliahan UIN2', 'informasi-perkuliahan-uin2', '<p>hhh<p><img style=\"width: 50%;\" data-filename=\"Screenshot_2017-12-07_22-55-59.png\" src=\"http://localhost:8000/assets/img/blogs/5a298b9057c7e.png\"></p><p>ssss</p><p><img data-filename=\"Screenshot_2017-12-04_17-27-17.png\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a298b9087f19.png\"></p><p><img style=\"width: 50%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a298b90c1e78.png\"></p><p>ddddd</p><p><img style=\"width: 50%;\" data-filename=\"errorr.png\" src=\"http://localhost:8000/assets/img/blogs/5a298b917be1e.png\"></p><p>hhhh</p><p><img style=\"width: 25%;\" data-filename=\"data.png\" src=\"http://localhost:8000/assets/img/blogs/5a298b91be434.png\"><br></p></p>\n', 'K0kuni', 0, NULL, '98', '0', 0, 0, '16650063', 1, '2017-12-07 11:42:25', '2017-12-08 14:58:54', '2017-12-08 14:58:54'),
(60, 'informasi perkuliahan UIN2', 'informasi-perkuliahan-uin2', '<p>hhh<p><img style=\"width: 50%;\" data-filename=\"Screenshot_2017-12-07_22-55-59.png\" src=\"http://localhost:8000/assets/img/blogs/5a298bd0837af.png\"></p><p>ssss</p><p><img data-filename=\"Screenshot_2017-12-04_17-27-17.png\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a298bd0b5339.png\"></p><p><img style=\"width: 50%;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a298bd0f0419.png\"></p><p>ddddd</p><p><img style=\"width: 50%;\" data-filename=\"errorr.png\" src=\"http://localhost:8000/assets/img/blogs/5a298bd1aa20c.png\"></p><p>hhhh</p><p><img style=\"width: 25%;\" data-filename=\"data.png\" src=\"http://localhost:8000/assets/img/blogs/5a298bd204f2c.png\"><br></p></p>\n', 'K0kuni', 0, NULL, '104', '0', 0, 1, '16650063', 1, '2017-12-07 11:43:30', '2017-12-08 15:32:01', '2017-12-08 15:32:01'),
(61, 'dhfdhfjdfhd', 'dhfdhfjdfhd', '<p>sdsdsdddddd<p><img style=\"width: 50%;\" data-filename=\"tanya4.png\" src=\"http://localhost:8000/assets/img/blogs/5a298bd22d9c3.png\"></p><p><img data-filename=\"tanya1.png\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a298bd25bc67.png\"><br></p></p>\n', 'K0kuni', 1, NULL, '104', '0', 0, 0, '16650063', 1, '2017-12-07 11:43:30', '2017-12-08 14:58:45', '2017-12-08 14:58:45'),
(62, 'informasi perkuliahan UIN', 'informasi-perkuliahan-uin', '<p>Saa<p><img data-filename=\"wifi.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a298e12e3f46.png\"></p><p><img data-filename=\"wordstar.jpg\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a298e13ac2da.jpeg\"></p><p><img style=\"width: 25%;\" data-filename=\"lotusos.jpg\" src=\"http://localhost:8000/assets/img/blogs/5a298e13ce6d7.jpeg\"></p><p>yhbhbh</p><p><img data-filename=\"Screenshot_2017-11-06_21-19-55.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a298e13daabb.png\"><br></p><p>                                  \r\n                               </p></p>\n', 'K0olga', 0, NULL, '112', '113,114,115,116', 0, 1, '16650063', 1, '2017-12-07 11:53:08', '2017-12-08 15:28:30', '2017-12-08 15:28:30'),
(63, 'asdd', 'asdd', '<p>jj</p>\n', 'K0olga', 1, NULL, '112,', '0', 0, 1, '16650063', 1, '2017-12-07 11:53:08', '2017-12-08 15:12:11', '2017-12-08 15:12:11'),
(64, 'Buber bersama keluarga pgmi', 'buber-bersama-keluarga-pgmi', '<p>ini acara buber pgmi</p>\n', 'K0hdrh', 0, NULL, '153', '', 0, 0, '16650063', 1, '2017-12-08 14:53:06', '2017-12-08 14:56:16', '2017-12-08 14:56:16'),
(65, 'Buber bersama keluarga pgmi eng', 'buber-bersama-keluarga-pgmi-eng', '<p>ini acara buber pgmi en</p>\n', 'K0hdrh', 1, NULL, '153', '', 0, 0, '16650063', 1, '2017-12-08 14:53:06', '2017-12-08 14:57:14', '2017-12-08 14:57:14'),
(66, 'informasi perkuliahan UIN sekar', 'informasi-perkuliahan-uin-sekar', '<p>heheheh<p>hahaha</p><p>pisss</p></p>\n', 'K0olga', 0, '2017-12-21', '157', '', 1, 1, '16650063', 1, '2017-12-08 18:47:30', '2017-12-09 05:52:20', NULL),
(67, 'informasi perkuliahan UIN sekar mina', 'informasi-perkuliahan-uin-sekar-mina', '<p>wekk, inggris, wekk</p>\n', 'K0olga', 1, '2017-12-21', '157', '', 0, 1, '16650063', 1, '2017-12-08 18:47:30', '2017-12-08 18:47:30', NULL),
(68, 'ini coba post dengan tanggal', 'ini-coba-post-dengan-tanggal', '<p>ini coba post dengan tanggal content</p>\n', 'K0hdrh', 0, '2017-12-07', '158', '', 0, 0, '16650063', 1, '2017-12-08 19:13:26', '2017-12-08 19:20:41', NULL),
(69, 'ini coba post dengan tanggal edited englis', 'ini-coba-post-dengan-tanggal-edited-englis', '<p>ini coba post dengan tanggal content engl</p>\n', 'K0hdrh', 1, '2017-12-06', '158', '', 0, 2, '16650063', 1, '2017-12-08 19:13:26', '2017-12-08 19:20:41', NULL),
(70, 'iki judulll', 'iki-judulll', '<p>iki contenttaaa<p><img style=\"width: 25%;\" data-filename=\"error.png\" src=\"http://localhost:8000/assets/img/blogs/5a2c873211294.png\"><br></p></p>\n', 'K0hmps', 0, '2022-02-02', '164', '165', 0, 1, '16650021', 1, '2017-12-09 18:00:34', '2017-12-09 18:00:34', NULL),
(71, 'yoi iki judul engls', 'yoi-iki-judul-engls', '<p>mantabbbb, haha<pre>echo \"Hello world\";</pre><blockquote>aa</blockquote><p><b><i><u>sss</u></i></b><img data-filename=\"data.png\" style=\"width: 50%;\" src=\"http://localhost:8000/assets/img/blogs/5a2c87668b442.png\"></p><p></p></p>\n', 'K0hmps', 1, NULL, '164', '166', 0, 1, '16650021', 1, '2017-12-09 18:00:34', '2017-12-09 18:01:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_en` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `judul`, `judul_en`, `tanggal`, `jam`, `aktif`, `created_at`, `updated_at`) VALUES
(2, 'Workshop Mengajar', 'Workshop teaching', '2017-01-11', '00:00:00', 1, '2017-08-03 17:00:00', '2017-11-02 17:00:00'),
(3, 'Kunjungan Industri', 'visit the insdustry', '2017-05-01', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(4, 'Lomba Futsal PGMI', 'Race Futsal', '2017-06-08', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(6, 'Seminar Internasional', 'Seminar International', '2017-10-12', '00:00:00', 1, '2017-11-30 18:50:38', '2017-11-30 18:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id_gallery` bigint(20) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id_gallery`, `gambar`, `kategori`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'a.jpg', '0', '2017-11-11 17:00:00', '2017-11-11 17:00:00', NULL),
(2, 'b.jpeg', '0', '2017-11-11 17:00:00', '2017-11-11 17:00:00', NULL),
(3, 'c.jpg', '0', '2017-11-11 17:00:00', '2017-11-11 17:00:00', NULL),
(4, 'd.jpg', '0', '2017-11-11 17:00:00', '2017-11-11 17:00:00', NULL),
(5, 'nama.jpg', '0', '2017-11-26 02:06:37', '2017-11-26 02:06:37', NULL),
(6, '1512096304.png', '0', '2017-11-26 02:08:26', '2017-11-26 02:08:26', NULL),
(7, 'nama.jpg', '0', '2017-11-26 11:37:08', '2017-11-26 11:37:08', NULL),
(8, '1511992528.png', '0', '2017-11-29 14:55:28', '2017-11-29 14:55:28', NULL),
(9, '1512097249.png', '0', '2017-11-30 20:00:49', '2017-11-30 20:00:49', NULL),
(26, '1512101020.png', '0', '2017-11-30 21:03:40', '2017-11-30 21:03:40', NULL),
(27, '1512109806.jpg', '0', '2017-11-30 23:30:06', '2017-11-30 23:30:06', NULL),
(28, '1512110013.png', '0', '2017-11-30 23:33:33', '2017-11-30 23:33:33', NULL),
(29, '1512112861.png', '0', '2017-12-01 00:21:01', '2017-12-01 00:21:01', NULL),
(30, '1512112969.png', '0', '2017-12-01 00:22:49', '2017-12-01 00:22:49', NULL),
(31, '1512113031.png', '0', '2017-12-01 00:23:51', '2017-12-01 00:23:51', NULL),
(32, '1512113072.png', '0', '2017-12-01 00:24:32', '2017-12-01 00:24:32', NULL),
(33, '1512113107.png', '0', '2017-12-01 00:25:07', '2017-12-01 00:25:07', NULL),
(34, '1512135390.png', '0', '2017-12-01 06:36:30', '2017-12-01 06:36:30', NULL),
(35, '1512135537.png', '0', '2017-12-01 06:38:57', '2017-12-01 06:38:57', NULL),
(36, '1512137325.png', '0', '2017-12-01 07:08:45', '2017-12-01 07:08:45', NULL),
(37, '1512137443.png', '0', '2017-12-01 07:10:43', '2017-12-01 07:10:43', NULL),
(38, '1512142710.png', '0', '2017-12-01 08:38:30', '2017-12-01 08:38:30', NULL),
(39, '1512263751.gif', '0', '2017-12-02 18:15:51', '2017-12-02 18:15:51', NULL),
(40, '1512265519.png', '0', '2017-12-02 18:45:19', '2017-12-02 18:45:19', NULL),
(41, '1512266755.png', '0', '2017-12-02 19:05:55', '2017-12-02 19:05:55', NULL),
(42, '1512266806.png', '0', '2017-12-02 19:06:46', '2017-12-02 19:06:46', NULL),
(43, '1512266890.png', '0', '2017-12-02 19:08:10', '2017-12-02 19:08:10', NULL),
(44, '1512267052.png', '0', '2017-12-02 19:10:52', '2017-12-02 19:10:52', NULL),
(45, '1512339698.png', '0', '2017-12-03 15:21:38', '2017-12-03 15:21:38', NULL),
(46, '1512339928.png', '0', '2017-12-03 15:25:28', '2017-12-03 15:25:28', NULL),
(47, '1512340018.jpg', '0', '2017-12-03 15:26:58', '2017-12-03 15:26:58', NULL),
(48, '1512340359.png', '0', '2017-12-03 15:32:39', '2017-12-03 15:32:39', NULL),
(49, '1512340486.png', '0', '2017-12-03 15:34:46', '2017-12-03 15:34:46', NULL),
(50, '1512341129.png', '0', '2017-12-03 15:45:29', '2017-12-03 15:45:29', NULL),
(51, 'cellular-education-classroom-159844.jpeg', '0', '2017-12-04 20:43:13', '2017-12-04 20:43:13', NULL),
(52, '1512567892.jpeg', '0', '2017-12-06 06:44:52', '2017-12-06 06:44:52', NULL),
(53, 'pexels-photo-448877.jpeg', '0', '2017-12-06 06:51:41', '2017-12-06 06:51:41', NULL),
(54, '1512571910.jpg', '0', '2017-12-06 07:51:50', '2017-12-06 07:51:50', NULL),
(55, '1512572025.jpg', '0', '2017-12-06 07:53:45', '2017-12-06 07:53:45', NULL),
(56, '1512617315.jpg', '0', '2017-12-06 20:28:35', '2017-12-06 20:28:35', NULL),
(57, '1512618476.jpg', '0', '2017-12-06 20:47:56', '2017-12-06 20:47:56', NULL),
(58, '1512620575.jpg', '0', '2017-12-06 21:22:55', '2017-12-06 21:22:55', NULL),
(59, '1512658008.png', '0', '2017-12-07 07:46:48', '2017-12-07 07:46:48', NULL),
(60, '5a2954588bade', '0', '2017-12-07 07:46:48', '2017-12-07 07:46:48', NULL),
(61, '5a295458bbfdc', '0', '2017-12-07 07:46:48', '2017-12-07 07:46:48', NULL),
(62, '1512658131.png', '0', '2017-12-07 07:48:51', '2017-12-07 07:48:51', NULL),
(63, '5a2954d3975e3', '0', '2017-12-07 07:48:51', '2017-12-07 07:48:51', NULL),
(64, '5a2954d3c5b4d', '0', '2017-12-07 07:48:51', '2017-12-07 07:48:51', NULL),
(65, '1512658310.png', '0', '2017-12-07 07:51:50', '2017-12-07 07:51:50', NULL),
(66, '5a2955868f24f', '0', '2017-12-07 07:51:50', '2017-12-07 07:51:50', NULL),
(67, '5a295586bd552', '0', '2017-12-07 07:51:50', '2017-12-07 07:51:50', NULL),
(68, '1512658342.png', '0', '2017-12-07 07:52:22', '2017-12-07 07:52:22', NULL),
(69, '5a2955a63cb60', '0', '2017-12-07 07:52:22', '2017-12-07 07:52:22', NULL),
(70, '5a2955a66c734', '0', '2017-12-07 07:52:22', '2017-12-07 07:52:22', NULL),
(71, '1512658485.png', '0', '2017-12-07 07:54:45', '2017-12-07 07:54:45', NULL),
(72, '5a29563548c1f', '0', '2017-12-07 07:54:45', '2017-12-07 07:54:45', NULL),
(73, '5a295635768e1', '0', '2017-12-07 07:54:45', '2017-12-07 07:54:45', NULL),
(74, '1512658691.png', '0', '2017-12-07 07:58:11', '2017-12-07 07:58:11', NULL),
(75, '5a295703c1f63', '0', '2017-12-07 07:58:11', '2017-12-07 07:58:11', NULL),
(76, '5a295703f04e4', '0', '2017-12-07 07:58:11', '2017-12-07 07:58:11', NULL),
(77, '1512659743.png', '0', '2017-12-07 08:15:43', '2017-12-07 08:15:43', NULL),
(78, '5a295b1f8a4bfpng', '0', '2017-12-07 08:15:43', '2017-12-07 08:15:43', NULL),
(79, '5a295b1fb87a1png', '0', '2017-12-07 08:15:43', '2017-12-07 08:15:43', NULL),
(80, '1512660338.png', '0', '2017-12-07 08:25:38', '2017-12-07 08:25:38', NULL),
(81, '5a295d725a2b0png', '0', '2017-12-07 08:25:38', '2017-12-07 08:25:38', NULL),
(82, '5a295d7287a32png', '0', '2017-12-07 08:25:38', '2017-12-07 08:25:38', NULL),
(83, '1512660451.png', '0', '2017-12-07 08:27:31', '2017-12-07 08:27:31', NULL),
(84, '5a295de318d3cpng', '0', '2017-12-07 08:27:31', '2017-12-07 08:27:31', NULL),
(85, '5a295de3475fepng', '0', '2017-12-07 08:27:31', '2017-12-07 08:27:31', NULL),
(86, '1512660537.png', '0', '2017-12-07 08:28:57', '2017-12-07 08:28:57', NULL),
(87, '5a295e398e08apng', '0', '2017-12-07 08:28:57', '2017-12-07 08:28:57', NULL),
(88, '5a295e39bb0d7png', '0', '2017-12-07 08:28:57', '2017-12-07 08:28:57', NULL),
(89, '1512660656.png', '0', '2017-12-07 08:30:56', '2017-12-07 08:30:56', NULL),
(90, '5a295eb10d6dfpng', '0', '2017-12-07 08:30:57', '2017-12-07 08:30:57', NULL),
(91, '5a295eb13b328png', '0', '2017-12-07 08:30:57', '2017-12-07 08:30:57', NULL),
(92, '1512660682.png', '0', '2017-12-07 08:31:22', '2017-12-07 08:31:22', NULL),
(93, '5a295ecae4d63png', '0', '2017-12-07 08:31:22', '2017-12-07 08:31:22', NULL),
(94, '5a295ecb1d5dbpng', '0', '2017-12-07 08:31:23', '2017-12-07 08:31:23', NULL),
(95, '1512660959.png', '0', '2017-12-07 08:35:59', '2017-12-07 08:35:59', NULL),
(96, '5a295fdf336f4.png', '0', '2017-12-07 08:35:59', '2017-12-07 08:35:59', NULL),
(97, '5a295fdf5f57b.png', 'K0hmps', '2017-12-07 08:35:59', '2017-12-07 08:35:59', NULL),
(98, '1512672144.png', 'K0kuni', '2017-12-07 11:42:24', '2017-12-07 11:42:24', NULL),
(99, '5a298b9057c7e.png', 'K0kuni', '2017-12-07 11:42:24', '2017-12-07 11:42:24', NULL),
(100, '5a298b9087f19.png', 'K0kuni', '2017-12-07 11:42:24', '2017-12-07 11:42:24', NULL),
(101, '5a298b90c1e78.png', 'K0kuni', '2017-12-07 11:42:24', '2017-12-07 11:42:24', NULL),
(102, '5a298b917be1e.png', 'K0kuni', '2017-12-07 11:42:25', '2017-12-07 11:42:25', NULL),
(103, '5a298b91be434.png', 'K0kuni', '2017-12-07 11:42:25', '2017-12-07 11:42:25', NULL),
(104, '1512672208.png', 'K0kuni', '2017-12-07 11:43:28', '2017-12-07 11:43:28', NULL),
(105, '5a298bd0837af.png', 'K0kuni', '2017-12-07 11:43:28', '2017-12-07 11:43:28', NULL),
(106, '5a298bd0b5339.png', 'K0kuni', '2017-12-07 11:43:28', '2017-12-07 11:43:28', NULL),
(107, '5a298bd0f0419.png', 'K0kuni', '2017-12-07 11:43:28', '2017-12-07 11:43:28', NULL),
(108, '5a298bd1aa20c.png', 'K0kuni', '2017-12-07 11:43:29', '2017-12-07 11:43:29', NULL),
(109, '5a298bd204f2c.png', 'K0kuni', '2017-12-07 11:43:30', '2017-12-07 11:43:30', NULL),
(110, '5a298bd22d9c3.png', 'K0kuni', '2017-12-07 11:43:30', '2017-12-07 11:43:30', NULL),
(111, '5a298bd25bc67.png', 'K0kuni', '2017-12-07 11:43:30', '2017-12-07 11:43:30', NULL),
(112, '1512672786.jpg', 'K0olga', '2017-12-07 11:53:06', '2017-12-07 11:53:06', NULL),
(113, '5a298e12e3f46.png', 'K0olga', '2017-12-07 11:53:06', '2017-12-07 11:53:06', NULL),
(114, '5a298e13ac2da.jpeg', 'K0olga', '2017-12-07 11:53:07', '2017-12-07 11:53:07', NULL),
(115, '5a298e13ce6d7.jpeg', 'K0olga', '2017-12-07 11:53:07', '2017-12-07 11:53:07', NULL),
(116, '5a298e13daabb.png', 'K0olga', '2017-12-07 11:53:07', '2017-12-07 11:53:07', NULL),
(117, '1512696456.png', 'prestasi', '2017-12-07 18:27:36', '2017-12-07 18:27:36', NULL),
(118, '1512696521.png', 'prestasi', '2017-12-07 18:28:41', '2017-12-07 18:28:41', NULL),
(119, '5a29eac9b38b1.png', 'prestasi', '2017-12-07 18:28:41', '2017-12-07 18:28:41', NULL),
(120, '5a29eaca0e2ed.png', 'prestasi', '2017-12-07 18:28:42', '2017-12-07 18:28:42', NULL),
(121, '5a29eacabc5bb.png', 'prestasi', '2017-12-07 18:28:42', '2017-12-07 18:28:42', NULL),
(122, '1512696610.png', 'prestasi', '2017-12-07 18:30:10', '2017-12-07 18:30:10', NULL),
(123, '5a29eb22de6be.png', 'prestasi', '2017-12-07 18:30:10', '2017-12-07 18:30:10', NULL),
(124, '5a29eb23257d7.png', 'prestasi', '2017-12-07 18:30:11', '2017-12-07 18:30:11', NULL),
(125, '5a29eb23d208f.png', 'prestasi', '2017-12-07 18:30:11', '2017-12-07 18:30:11', NULL),
(126, '1512696664.png', 'prestasi', '2017-12-07 18:31:04', '2017-12-07 18:31:04', NULL),
(127, '5a29eb59010f5.png', 'prestasi', '2017-12-07 18:31:05', '2017-12-07 18:31:05', NULL),
(128, '5a29eb5939979.png', 'prestasi', '2017-12-07 18:31:05', '2017-12-07 18:31:05', NULL),
(129, '5a29eb59e72ab.png', 'prestasi', '2017-12-07 18:31:05', '2017-12-07 18:31:05', NULL),
(130, '1512696866.png', 'prestasi', '2017-12-07 18:34:26', '2017-12-07 18:34:26', NULL),
(131, '5a29ec2262916.png', 'prestasi', '2017-12-07 18:34:26', '2017-12-07 18:34:26', NULL),
(132, '5a29ec229c371.png', 'prestasi', '2017-12-07 18:34:26', '2017-12-07 18:34:26', NULL),
(133, '5a29ec2353202.png', 'prestasi', '2017-12-07 18:34:27', '2017-12-07 18:34:27', NULL),
(134, '1512697100.png', 'prestasi', '2017-12-07 18:38:20', '2017-12-07 18:38:20', NULL),
(135, '5a29ed0d068ea.png', 'prestasi', '2017-12-07 18:38:21', '2017-12-07 18:38:21', NULL),
(136, '5a29ed0d38fa1.png', 'prestasi', '2017-12-07 18:38:21', '2017-12-07 18:38:21', NULL),
(137, '5a29ed0d69097.png', 'K0prts', '2017-12-07 18:38:21', '2017-12-07 18:38:21', NULL),
(138, '5a29ed0d9f00c.png', 'K0prts', '2017-12-07 18:38:21', '2017-12-07 18:38:21', NULL),
(139, '1512707753.png', 'K0prts', '2017-12-07 21:35:54', '2017-12-07 21:35:54', NULL),
(140, '5a2a16aa0c029.png', 'K0prts', '2017-12-07 21:35:54', '2017-12-07 21:35:54', NULL),
(141, '5a2a16aa3f0e5.png', 'K0prts', '2017-12-07 21:35:54', '2017-12-07 21:35:54', NULL),
(142, '1512707959.png', 'K0prts', '2017-12-07 21:39:19', '2017-12-07 21:39:19', NULL),
(143, '5a2a1777d697d.png', 'K0prts', '2017-12-07 21:39:19', '2017-12-07 21:39:19', NULL),
(144, '5a2a177814c71.png', 'K0prts', '2017-12-07 21:39:20', '2017-12-07 21:39:20', NULL),
(145, '5a2a1778d296a.png', 'K0prts', '2017-12-07 21:39:20', '2017-12-07 21:39:20', NULL),
(146, '1512719692.png', 'K0prts', '2017-12-08 00:54:52', '2017-12-08 00:54:52', NULL),
(147, '5a2a454cb80e9.png', 'K0prts', '2017-12-08 00:54:52', '2017-12-08 00:54:52', NULL),
(148, '5a2b05ddb66d1.png', 'K0prts', '2017-12-08 14:36:29', '2017-12-08 14:36:29', NULL),
(149, '5a2b070d5a238.png', 'K0prts', '2017-12-08 14:41:33', '2017-12-08 14:41:33', NULL),
(150, '5a2b070d910ad.png', 'K0prts', '2017-12-08 14:41:33', '2017-12-08 14:41:33', NULL),
(151, '5a2b074c2d354.png', 'K0prts', '2017-12-08 14:42:36', '2017-12-08 14:42:36', NULL),
(152, '5a2b074c659a8.png', 'K0prts', '2017-12-08 14:42:36', '2017-12-08 14:42:36', NULL),
(153, '1512769986.png', 'K0hdrh', '2017-12-08 14:53:06', '2017-12-08 14:53:06', NULL),
(154, '5a2b230155e2a.png', 'K0hmps', '2017-12-08 16:40:49', '2017-12-08 16:40:49', NULL),
(155, '5a2b26ff063e3.png', 'K0hmps', '2017-12-08 16:57:51', '2017-12-08 16:57:51', NULL),
(156, '1512777773.png', 'K0sldr', '2017-12-08 17:02:53', '2017-12-08 17:02:53', NULL),
(157, '1512784050.png', 'K0olga', '2017-12-08 18:47:30', '2017-12-08 18:47:30', NULL),
(158, '1512785606.png', 'K0hdrh', '2017-12-08 19:13:26', '2017-12-08 19:13:26', NULL),
(159, '1512827659.png', 'K0hdrh', '2017-12-09 06:54:19', '2017-12-09 06:54:19', NULL),
(160, '5a2beb0b99f61.jpeg', 'K0hdrh', '2017-12-09 06:54:19', '2017-12-09 06:54:19', NULL),
(161, '5a2beb0bc1584.png', 'K0hdrh', '2017-12-09 06:54:19', '2017-12-09 06:54:19', NULL),
(162, '1512866057.png', 'prestasi', '2017-12-09 17:34:17', '2017-12-09 17:34:17', NULL),
(163, '1512866245.png', 'prestasi', '2017-12-09 17:37:25', '2017-12-09 17:37:25', NULL),
(164, '1512867634.png', 'K0hmps', '2017-12-09 18:00:34', '2017-12-09 18:00:34', NULL),
(165, '5a2c873211294.png', 'K0hmps', '2017-12-09 18:00:34', '2017-12-09 18:00:34', NULL),
(166, '5a2c87668b442.png', 'K0hmps', '2017-12-09 18:01:26', '2017-12-09 18:01:26', NULL),
(167, '1512868062.png', 'prestasi', '2017-12-09 18:07:42', '2017-12-09 18:07:42', NULL),
(168, '1512868169.png', 'prestasi', '2017-12-09 18:09:29', '2017-12-09 18:09:29', NULL),
(169, '1512868220.png', 'prestasi', '2017-12-09 18:10:20', '2017-12-09 18:10:20', NULL),
(170, '1512868414.png', 'prestasi', '2017-12-09 18:13:34', '2017-12-09 18:13:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kategories`
--

CREATE TABLE `kategories` (
  `id_kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaEn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategories`
--

INSERT INTO `kategories` (`id_kategori`, `nama`, `namaEn`, `gol`, `created_at`, `updated_at`, `deleted_at`) VALUES
('K0hdrh', 'KAJIAN', 'Study', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0hmps', 'HMPS', 'HMPS', '', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0klta', 'Kesenian', 'art', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0kuni', 'Kunjungan', 'Tour', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0mkrb', 'Keakraban', 'Familiarity', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0olga', 'Olahraga', 'Sport', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0ppm', 'PPM', 'PPM', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0prts', 'Prestasi', 'achievement', '', '2017-12-07 17:00:00', '2017-12-07 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_11_10_140832_create_makrabs_table', 2),
(13, '2017_11_10_142600_create_makrabs_table', 3),
(14, '2017_11_10_150422_create_blogs_table', 4),
(15, '2017_11_10_151312_create_kategories_table', 5),
(16, '2017_11_10_152429_create_galleries_table', 6),
(17, '2017_11_10_234910_create_events_table', 7),
(18, '2017_11_11_154858_create_navs_table', 8),
(19, '2017_12_04_004239_create_notifikations_table', 9),
(20, '2017_12_07_223458_create_table_prestasi', 10);

-- --------------------------------------------------------

--
-- Table structure for table `navs`
--

CREATE TABLE `navs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nav` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subnav` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navs`
--

INSERT INTO `navs` (`id`, `nav`, `subnav`, `bahasa`, `created_at`, `updated_at`) VALUES
(1, 'BERANDA', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(2, 'HMPS', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(3, 'HOME', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(4, 'HMPS', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(5, 'KEGIATAN', '', 0, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(6, 'ACTIVITIES', '', 1, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(7, 'CARA UPLOAD', '', 0, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(8, 'GUIDE UPLOAD', '', 1, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(9, 'EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(10, 'EKSTRA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(11, 'TOUR', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(12, 'KUNJUNGAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(13, 'SPORT', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(14, 'OLAHRAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(15, 'KESENIAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(16, 'ART', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(17, 'GALERI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(18, 'GALLERY', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(19, 'Cari', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(20, 'Search', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(21, 'KEGIATAN MAHASISWA PGMI UIN SUNAN KALIJAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(22, 'ACTIVITY STUDENT PGMI UIN SUNAN KALIJAGA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(23, 'POPULAR POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(24, 'POST POPULER', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(25, 'Selengkapnya', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(26, 'Read More', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(27, 'EKSTRA MAHASISWA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(28, 'STUDENT EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(29, 'Kategori', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(30, 'Category', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(31, 'POST TERBARU', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(32, 'NEW POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(33, 'Date', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(34, 'Tanggal', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(35, 'Created By', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(36, 'Dibuat Oleh', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(37, 'AGENDA TERKINI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(38, 'NEW EVENTS', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(39, 'MOST CONTRIBUTE', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(40, 'KONTRIBUTOR TERAKTIF', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(41, 'PRESTASI', '', 0, NULL, NULL),
(42, 'ACHIEVEMENT', '', 1, NULL, NULL),
(43, 'KEAKRABAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(44, 'FAMILIARITY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(45, 'KAJIAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(46, 'STUDY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(47, 'POST TERKAIT', '', 0, '2017-12-15 17:00:00', '2017-12-15 17:00:00'),
(48, 'RELATED POST', '', 1, '2017-12-09 17:00:00', '2017-12-09 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notifikations`
--

CREATE TABLE `notifikations` (
  `id` int(10) UNSIGNED NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_content` int(11) NOT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifikations`
--

INSERT INTO `notifikations` (`id`, `pesan`, `kd_content`, `to`, `baca`, `created_at`, `updated_at`) VALUES
(1, 'Selamat Post Anda Yang Berjudul <b>pelantikan ayub menjadi CEO JustFriend week</b> Diterima, Silahkan Cek di Website', 13, '16650063', 1, '2017-12-03 19:41:04', '2017-12-03 20:04:57'),
(2, 'Selamat Post Anda Yang Berjudul <b>title post from ayyub</b> Diterima, Silahkan Cek di Website', 26, '16650063', 1, '2017-12-04 21:00:19', '2017-12-04 21:02:09'),
(3, 'Selamat Post Anda Yang Berjudul <b>judul Post dari ayyub2</b> Diterima, Silahkan Cek di Website', 27, '16650063', 1, '2017-12-04 21:00:53', '2017-12-04 21:02:09'),
(4, 'Selamat Post Anda Yang Berjudul <b>judul Post dari ayyub2</b> Diterima, Silahkan Cek di Website', 27, '16650063', 1, '2017-12-04 21:01:05', '2017-12-04 21:02:09'),
(5, 'Selamat Post Anda Yang Berjudul <b>title post from ayyub2</b> Diterima, Silahkan Cek di Website', 28, '16650063', 1, '2017-12-04 21:01:31', '2017-12-04 21:02:09'),
(6, 'Selamat Post Anda Yang Berjudul <b>Seminar International 3</b> Diterima, Silahkan Cek di Website', 40, '16650063', 1, '2017-12-06 20:29:30', '2017-12-06 20:34:31'),
(7, 'Selamat Post Anda Yang Berjudul <b>Seminar Internasional 3</b> Diterima, Silahkan Cek di Website', 39, '16650063', 1, '2017-12-06 20:29:34', '2017-12-06 20:34:31'),
(8, 'Selamat Post Anda Yang Berjudul <b>Baksos Bahasa Indo</b> Diterima, Silahkan Cek di Website', 41, '16650063', 1, '2017-12-06 20:48:42', '2017-12-08 17:55:40'),
(9, 'Selamat Post Anda Yang Berjudul <b>Baksos Bahasa Inggris</b> Diterima, Silahkan Cek di Website', 42, '16650063', 1, '2017-12-06 20:48:46', '2017-12-08 17:55:40'),
(10, 'Selamat Post Anda Yang Berjudul <b>informasi perkuliahan UIN</b> Diterima, Silahkan Cek di Website', 57, '16650063', 1, '2017-12-07 08:37:15', '2017-12-08 17:55:40'),
(11, 'Selamat Post Anda Yang Berjudul <b>informasi perkuliahan UIN2</b> Diterima, Silahkan Cek di Website', 60, '16650063', 1, '2017-12-07 11:44:01', '2017-12-08 17:55:40'),
(12, 'Selamat Post Anda Yang Berjudul <b>asdd</b> Diterima, Silahkan Cek di Website', 63, '16650063', 1, '2017-12-07 11:53:21', '2017-12-08 17:55:40'),
(13, 'Selamat Post Anda Yang Berjudul <b>informasi perkuliahan UIN</b> Diterima, Silahkan Cek di Website', 62, '16650063', 1, '2017-12-07 11:53:28', '2017-12-08 17:55:40'),
(14, 'Selamat Post Anda Yang Berjudul <b>informasi perkuliahan UIN</b> 2, Silahkan Cek di Website', 54, '16650063', 1, '2017-12-08 17:55:23', '2017-12-08 17:55:40'),
(15, 'Selamat Post Anda Yang Berjudul <b>Seminar Nasional pak ayyub2 haha</b> berhasil diedit, Silahkan Cek di Website', 31, '16650063', 1, '2017-12-08 18:08:55', '2017-12-08 18:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peserta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sifat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bhs` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgDesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `judul`, `slug`, `peserta`, `sifat`, `jenjang`, `content`, `bhs`, `tanggal`, `img`, `imgDesc`, `kategori`, `view`, `aktif`, `from`, `baca`, `created_at`, `updated_at`) VALUES
(3, 'Lomba cook rice goreng', 'lomba-cook-rice-goreng', 'yakin, bayu', 'Group', 'Province', '<p>wkwkw<p><img data-filename=\"errorr.png\" style=\"width: 360.5px;\" src=\"http://localhost:8000/assets/img/blogs/5a29ed0d9f00c.png\"><br></p></p>\n', 1, '2017-12-01', '134', '138', 'K0prts', 0, 0, '16650063', 0, '2017-12-07 18:38:21', '2017-12-07 18:38:21'),
(4, 'Lomba memasak Nasi goreng2 edited', 'lomba-memasak-nasi-goreng2-edited', 'chulis', 'Group', 'Kabupaten', '<blockquote>haha<p>ahah</p><p><img data-filename=\"Screenshot_2017-12-06_16-32-13.png\" style=\"width: 25%;\" src=\"http://localhost:8000/assets/img/blogs/5a2a1777d697d.png\"></p><p>haha</p><p><img style=\"width: 360.5px;\" data-filename=\"Screenshot_2017-12-06_16-31-51.png\" src=\"http://localhost:8000/assets/img/blogs/5a2a177814c71.png\"><br></p><p></p></blockquote>\n', 0, '2017-01-01', '142', '', 'K0prts', 5, 1, '16650063', 0, '2017-12-07 21:39:20', '2017-12-09 15:56:07'),
(5, 'rasyidikkkk', 'rasyidikkkk', 'syidik', 'Individu', 'Kabupaten', '<p>gantengggg tenan</p>\n', 0, '2022-12-02', '163', '', 'K0prts', 0, 0, '16650021', 0, '2017-12-09 17:37:25', '2017-12-09 18:03:55'),
(6, 'rasyidikkk en', 'rasyidikkk-en', 'syidik', 'Individu', 'Kabupaten', '<p>pollllll&nbsp; &nbsp; hehe</p>\n', 1, '2022-12-02', '163', '', 'K0prts', 0, 0, '16650021', 0, '2017-12-09 17:37:25', '2017-12-09 17:37:25'),
(7, 'Lomba memasak Nasi bakar3 indone', 'lomba-memasak-nasi-bakar3-indone', 'sekar, ayub', 'Individu', 'Kabupaten', '<p>content Lomba memasak Nasi bakar3 indone</p>\n', 0, '2018-03-03', '170', '', 'K0prts', 0, 0, '16650021', 1, '2017-12-09 18:13:34', '2017-12-09 18:13:34'),
(8, 'Lomba memasak Nasi bakar3 english', 'lomba-memasak-nasi-bakar3-english', 'sekar, ayub', 'Individu', 'Kabupaten', '<p>content Lomba memasak Nasi bakar3 english<p><br></p><table class=\"table table-bordered\"><tbody><tr><td>judul</td><td>content</td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td></tr></tbody></table><p><br></p></p>\n', 1, '2018-03-03', '170', '', 'K0prts', 0, 0, '16650021', 1, '2017-12-09 18:13:34', '2017-12-09 18:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `id_gambar` int(11) NOT NULL,
  `captionId` text NOT NULL,
  `captionEn` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `id_gambar`, `captionId`, `captionEn`, `created_at`, `updated_at`) VALUES
(2, 52, 'Caption2Idhe', 'Caption2En', '2017-11-26 17:00:00', '2017-12-06 06:44:52'),
(3, 53, 'Caption3Id', 'Caption3En', '2017-11-27 17:00:00', '2017-12-06 06:51:41'),
(4, 51, 'Seminar Nasional', 'Seminar National', '2017-11-30 20:00:49', '2017-11-30 20:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sekar', 'sekar@pgmi', 'sekarpgmi', '$2y$10$iXAPIrZykXz8LEqXD9GUAOxSZDWcGlsZ7l97lVIPu.m231R.Svyza', 'E14ZIFwU4GpPOt16LW2sEgdnzruUSOAufAVKsqbJYw8UGQatdXiWF6gbAjMY', '2017-12-08 22:08:29', '2017-12-08 22:08:29'),
(2, 'ayyub', 'ayyub@pgmi', 'ayyubpgmi', '$2y$10$OiApDU4/06NLowtIwL9Uj.UMTh1RDjlYhcEOIU9bVsUPpKafJjNtC', 'QQBNijOyC5l4s7QjF8L4FpGq2lskdUsVnZkHecCz2nSRUBdBJq5sD7B6gycN', '2017-12-08 22:24:57', '2017-12-08 22:24:57'),
(3, 'pgmi', 'superpgmi', 'superpgmi', '$2y$10$EvWhF2qJftsPMaBB0jbWSOCXAtj9pUrXpub3TSwKvdBBllIDwr4HK', 'xP2KmmAkzPJsQ82uswzRIkctHzt9D25JOFPOv2pLCw4DukLHKXKHSVeeFfsL', '2017-12-08 23:45:48', '2017-12-08 23:45:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `kategories`
--
ALTER TABLE `kategories`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navs`
--
ALTER TABLE `navs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikations`
--
ALTER TABLE `notifikations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id_gallery` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `navs`
--
ALTER TABLE `navs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `notifikations`
--
ALTER TABLE `notifikations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
