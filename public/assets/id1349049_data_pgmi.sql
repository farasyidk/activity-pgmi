-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 10, 2017 at 03:56 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id1349049_data_pgmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `img` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgContent` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` bigint(20) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_en` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `judul`, `judul_en`, `tanggal`, `jam`, `aktif`, `created_at`, `updated_at`) VALUES
(2, 'Workshop Mengajar', 'Workshop teaching', '2017-01-11', '00:00:00', 1, '2017-08-03 17:00:00', '2017-11-02 17:00:00'),
(3, 'Kunjungan Industri', 'visit the insdustry', '2017-05-01', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(4, 'Lomba Futsal PGMI', 'Race Futsal', '2017-06-08', '00:00:00', 1, '2017-10-31 17:00:00', '2017-10-31 17:00:00'),
(6, 'Seminar Internasional', 'Seminar International', '2017-10-12', '00:00:00', 1, '2017-11-30 18:50:38', '2017-11-30 18:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id_gallery` bigint(20) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategories`
--

CREATE TABLE `kategories` (
  `id_kategori` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namaEn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gol` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategories`
--

INSERT INTO `kategories` (`id_kategori`, `nama`, `namaEn`, `gol`, `created_at`, `updated_at`, `deleted_at`) VALUES
('K0hdrh', 'KAJIAN', 'Study', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0hmps', 'HMPS', 'HMPS', '', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0klta', 'Kesenian', 'art', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0kuni', 'Kunjungan', 'Tour', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0mkrb', 'Keakraban', 'Familiarity', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0olga', 'Olahraga', 'Sport', 'Ekstra', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0ppm', 'PPM', 'PPM', 'Kegiatan', '2017-11-09 17:00:00', '2017-11-09 17:00:00', NULL),
('K0prts', 'Prestasi', 'achievement', '', '2017-12-07 17:00:00', '2017-12-07 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2014_10_12_000000_create_users_table', 1),
(11, '2014_10_12_100000_create_password_resets_table', 1),
(12, '2017_11_10_140832_create_makrabs_table', 2),
(13, '2017_11_10_142600_create_makrabs_table', 3),
(14, '2017_11_10_150422_create_blogs_table', 4),
(15, '2017_11_10_151312_create_kategories_table', 5),
(16, '2017_11_10_152429_create_galleries_table', 6),
(17, '2017_11_10_234910_create_events_table', 7),
(18, '2017_11_11_154858_create_navs_table', 8),
(19, '2017_12_04_004239_create_notifikations_table', 9),
(20, '2017_12_07_223458_create_table_prestasi', 10);

-- --------------------------------------------------------

--
-- Table structure for table `navs`
--

CREATE TABLE `navs` (
  `id` int(10) UNSIGNED NOT NULL,
  `nav` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subnav` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahasa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navs`
--

INSERT INTO `navs` (`id`, `nav`, `subnav`, `bahasa`, `created_at`, `updated_at`) VALUES
(1, 'BERANDA', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(2, 'HMPS', '', 0, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(3, 'HOME', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(4, 'HMPS', '', 1, '2017-11-10 17:00:00', '2017-11-10 17:00:00'),
(5, 'KEGIATAN', '', 0, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(6, 'ACTIVITIES', '', 1, '2017-11-11 17:00:00', '2017-11-11 17:00:00'),
(7, 'CARA UPLOAD', '', 0, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(8, 'GUIDE UPLOAD', '', 1, '2017-11-15 17:00:00', '2017-11-15 17:00:00'),
(9, 'EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(10, 'EKSTRA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(11, 'TOUR', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(12, 'KUNJUNGAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(13, 'SPORT', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(14, 'OLAHRAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(15, 'KESENIAN', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(16, 'ART', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(17, 'GALERI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(18, 'GALLERY', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(19, 'Cari', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(20, 'Search', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(21, 'KEGIATAN MAHASISWA PGMI UIN SUNAN KALIJAGA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(22, 'ACTIVITY STUDENT PGMI UIN SUNAN KALIJAGA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(23, 'POPULAR POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(24, 'POST POPULER', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(25, 'Selengkapnya', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(26, 'Read More', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(27, 'EKSTRA MAHASISWA', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(28, 'STUDENT EXTRA', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(29, 'Kategori', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(30, 'Category', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(31, 'POST TERBARU', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(32, 'NEW POST', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(33, 'Date', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(34, 'Tanggal', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(35, 'Created By', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(36, 'Dibuat Oleh', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(37, 'AGENDA TERKINI', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(38, 'NEW EVENTS', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(39, 'MOST CONTRIBUTE', '', 1, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(40, 'KONTRIBUTOR TERAKTIF', '', 0, '2017-12-04 17:00:00', '2017-12-04 17:00:00'),
(41, 'PRESTASI', '', 0, NULL, NULL),
(42, 'ACHIEVEMENT', '', 1, NULL, NULL),
(43, 'KEAKRABAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(44, 'FAMILIARITY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(45, 'KAJIAN', '', 0, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(46, 'STUDY', '', 1, '2017-12-08 17:00:00', '2017-12-08 17:00:00'),
(47, 'POST TERKAIT', '', 0, '2017-12-15 17:00:00', '2017-12-15 17:00:00'),
(48, 'RELATED POST', '', 1, '2017-12-09 17:00:00', '2017-12-09 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notifikations`
--

CREATE TABLE `notifikations` (
  `id` int(10) UNSIGNED NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_content` int(11) NOT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peserta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sifat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bhs` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imgDesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` int(11) NOT NULL,
  `aktif` int(11) NOT NULL,
  `from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `id_gambar` int(11) NOT NULL,
  `captionId` text NOT NULL,
  `captionEn` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `id_gambar`, `captionId`, `captionEn`, `created_at`, `updated_at`) VALUES
(2, 52, 'Caption2Idhe', 'Caption2En', '2017-11-26 17:00:00', '2017-12-06 06:44:52'),
(3, 53, 'Caption3Id', 'Caption3En', '2017-11-27 17:00:00', '2017-12-06 06:51:41'),
(4, 51, 'Seminar Nasional', 'Seminar National', '2017-11-30 20:00:49', '2017-11-30 20:00:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sekar', 'sekar@pgmi', 'sekarpgmi', '$2y$10$iXAPIrZykXz8LEqXD9GUAOxSZDWcGlsZ7l97lVIPu.m231R.Svyza', 'E14ZIFwU4GpPOt16LW2sEgdnzruUSOAufAVKsqbJYw8UGQatdXiWF6gbAjMY', '2017-12-08 22:08:29', '2017-12-08 22:08:29'),
(2, 'ayyub', 'ayyub@pgmi', 'ayyubpgmi', '$2y$10$OiApDU4/06NLowtIwL9Uj.UMTh1RDjlYhcEOIU9bVsUPpKafJjNtC', 'QQBNijOyC5l4s7QjF8L4FpGq2lskdUsVnZkHecCz2nSRUBdBJq5sD7B6gycN', '2017-12-08 22:24:57', '2017-12-08 22:24:57'),
(3, 'pgmi', 'superpgmi', 'superpgmi', '$2y$10$EvWhF2qJftsPMaBB0jbWSOCXAtj9pUrXpub3TSwKvdBBllIDwr4HK', 'xP2KmmAkzPJsQ82uswzRIkctHzt9D25JOFPOv2pLCw4DukLHKXKHSVeeFfsL', '2017-12-08 23:45:48', '2017-12-08 23:45:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id_gallery`);

--
-- Indexes for table `kategories`
--
ALTER TABLE `kategories`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navs`
--
ALTER TABLE `navs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifikations`
--
ALTER TABLE `notifikations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id_gallery` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `navs`
--
ALTER TABLE `navs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `notifikations`
--
ALTER TABLE `notifikations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
