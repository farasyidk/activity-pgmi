// jQuery & Velocity.js

function slideUpIn() {
  $("#login").velocity("transition.slideUpIn", 1250)
};

function slideLeftIn() {
  $(".row").delay(500).velocity("transition.slideLeftIn", {stagger: 500})
}

function shake() {
  $(".password-row").velocity("callout.shake");
}
function shake2() {
  $(".username-row").velocity("callout.shake");
}
slideUpIn();
slideLeftIn();
$("#login-button").on("click", function () {
  if ($('input[name=email]').val() == "") {
      shake();
  }
  if ($('input[name=password]').val() == "") {
      shake2();
  }
});
