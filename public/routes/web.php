<?php
Route::get('/adminn', 'AdminController@home');
Route::get('/adminn/all-post', array('as'=>'all', 'uses'=>'AdminController@SemuaPost'));
Route::view('/adminn/add-post', 'admin.add-post');
Route::get('/adminn/edit-post/{id}', array('as'=>'editPost', 'uses'=>'AdminController@EditPost'));
Route::post('/adminn/edit-post/', array('as'=>'editPost', 'uses'=>'AdminController@sEditPost'));
Route::post('/adminn/add-post', array('as'=>'SimpanPost', 'uses'=>'AdminController@TambahPost'));
Route::delete('/adminn/destroy-post/', array('as'=>'HapusPost', 'uses'=>'AdminController@DestroyPost'));
Route::get('/adminn/confirm-post', array('as'=>'SetujuPost', 'uses'=>'AdminController@SetujuPost'));
Route::get('/adminn/confirmed/{data?}', array('as'=>'Setujui', 'uses'=>'AdminController@Setujukan'));
Route::get('/adminn/all-slider', array('as'=>'slider', 'uses'=>'AdminController@SliderPost'));
Route::view('/adminn/add-slider', 'admin.add-slider');
Route::post('/adminn/add-slider', array('as'=>'SimpanSlider', 'uses'=>'AdminController@TambahSlider'));
Route::get('/adminn/edit-slider/{data}', array('as'=>'eSlider', 'uses'=>'AdminController@EditSlider'));
Route::post('/adminn/edit-slider', array('as'=>'eSlider', 'uses'=>'AdminController@sEditSlider'));
Route::delete('/adminn/destroy-slider', array('as'=>'HapusSlider', 'uses'=>'AdminController@DestroySlider'));

Route::get('/mhs/{bhs?}', ['as'=>'mhs','uses'=>'MahaController@index']);
Route::get('/mhs/{bhs?}/all-post', ['as'=>'mhsAll', 'uses'=>'MahaController@allPost']);
Route::get('/dataAll-post/{data?}', ['as'=>'mhsData', 'uses'=>'MahaController@dataAllPost']);
Route::get('/mhs/{bhs?}/add-post', ['as'=>'mhsAdd', 'uses'=>'MahaController@addPost']);
Route::post('/mhs/add-post', ['as'=>'sAdd', 'uses'=>'MahaController@sAddPost']);
Route::get('/mhs/{bhs?}/edit-post/{data}', ['as'=>'mhsEdit', 'uses'=>'MahaController@editPost']);
Route::post('/mhs/edit-post/', ['as'=>'sEdit', 'uses'=>'MahaController@sEditPost']);
Route::get('/mhs/{bhs?}/destroy-post/{data}', array('as'=>'hPost', 'uses'=>'MahaController@DestroyPost'));
Route::get('/mhs/{bhs?}/notif', ['as'=>'notif','uses'=>'MahaController@notifikasi']);

Route::get('/adminn/all-events', array('as'=>'Agenda', 'uses'=>'AdminController@Agenda'));
Route::view('/adminn/add-event', 'admin.add-agenda');
Route::post('/adminn/add-event', array('as'=>'SimpanAgenda', 'uses'=>'AdminController@TambahAgenda'));
Route::delete('/adminn/destroy-agenda', array('as'=>'HapusAgenda', 'uses'=>'AdminController@DestroyAgenda'));
Route::get('/adminn/edit-event/{data}', 'AdminController@EditAgenda');
Route::post('/adminn/edit-event/', ['as'=>'eEvent', 'uses'=>'AdminController@sEditAgenda']);

Route::get('/{bhs?}/gallery', array('as'=>'gallery','uses'=>'BlogController@gallery'));
//hmps,guide
Route::get('/{bhs?}/result', 'BlogController@search');
Route::get("/{bhs?}/{hmps}",array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get("/{bhs?}/{page='cara upload'}",array('as'=>'blogs','uses'=>'BlogController@blogs'));
//kegiatan,ekstra
Route::get('/{bhs?}/kegiatan/{page}',array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get('/{bhs?}/Kegiatan/{slug}', 'BlogController@post');
Route::get('/{bhs?}/ekstra/{page}',array('as'=>'blogs','uses'=>'BlogController@blogs'));
Route::get('/{bhs?}/Ekstra/{slug}', 'BlogController@post');
Route::get('/{bhs?}/HMPS/{slug}', 'BlogController@post');
//Route::get("blogs/{page}/{bhs?}", 'BlogController@blogs');

Route::get('/{bhs?}', 'BlogController@index');
