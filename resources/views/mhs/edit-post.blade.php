@section('js')
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function () {
        readURL(this);
    });
    $(document).ready( function() {
      $("#editor3").summernote({
        height : 270,
        tabsize: 2,
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
        ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
        ['table', ['table']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
      ]});
    });
</script>

@stop
@extends('mhs.maha')
@section('title', 'Edit Post')

@section('content')
   <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
       <h3 style="padding-left:15px">EDIT POST</h3>
       <div class="underscore" style="margin-left:0px;margin-left:15px;margin-bottom:15px;"></div>
       {!! Form::open(array('route'=>'sEdit','files'=>true)) !!}
       <input type="hidden" name="data" value="{{$blog[0]['id']}}">
       <div class="col-md-12">
           <div class="box-header">
               <div class="form-group">
                       <label for="judulPost">Judul Post<sup style="color:red">*</sup></label>
                       <input type="text" name="judul" class="form-control" id="jdlPost" value="{{$blog[0]['judul']}}">
               </div>
           </div>
       </div>
       <div class="col-md-12">

             <div class="col-md-6" style="padding:0">
                <div class="form-group col-sm-12" style="padding-left:0">
                    <label for="judulPost">Isi Post<sup style="color:red">*</sup></label>

                                <textarea id="editor3" name="content" rows="20" cols="80">
                                   {{$blog[0]['content']}}
                                </textarea>

               </div>
             </div>
             <div class="col-md-6" style="padding:0">
               <div class="form-group col-sm-12">
                 <label for="disabledInput" class="control-label">Gambar Post</label><br />
                  <img src="{{asset('assets/img/blogs/'.$blog[0]['gambar'])}}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
                   <input type="file" id="gambar" name="gambarPost" accept="image/*" data-type='image'>

               </div>
               <div class="form-group col-sm-12">
                 <label for="disabledInput" class="control-label">Kategori Post</label>
                    <select class="form-control" name="kategori">
                      <option value="{{$blog[0]['id_kategori']}}">{{$blog[0]['nama']}}</option>
                      @foreach ($ktgr as $kt)
                         <option value="{{$kt->id_kategori}}">{{$kt->nama}}</option>
                      @endforeach
                    </select>


               </div>
               <div class="form-group col-sm-12">
                 <label class="control-label">Tanggal Kegiatan</label>
                   <input type="date" class="form-control" value="{{$blog[0]['tanggal']}}" type="text" placeholder="Indonesia" name="date">
               </div>
               <div class="form-group col-sm-12">
                 <label for="disabledInput" class="control-label">Bahasa Post</label>
                   <input class="form-control" id="disabledInput" type="text" placeholder="{{$blog[0]['bahasa'] == 0 ? 'Indonesia' : 'English'}}" disabled>

               </div>
            </div>

       </div>
       <div class="col-sm-12" style="margin-bottom:15px;padding-left:25px;">
          <input type="submit" style="margin-top:20px" class="btn btn-success" value="Update">
        </div>
   </div>
{{csrf_field()}}
{!! Form::close() !!}
@endsection
