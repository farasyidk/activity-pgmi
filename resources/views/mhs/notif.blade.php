@extends('mhs.maha')
@section('title', 'Pemberitahuan');
@section('content')

    <div class="col-sm-12 table-responsive">
        <h2 style="margin-top:0" id="selamat-mhs">Pemberitahuan</h2>
        <div class="underscore" style="margin-left:0px;margin-right:0px"></div>
        <div class="post-all">
            @include('mhs.data-notif')
        </div>
    </div>
@endsection
