@section('js')
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">

$(document).ready( function() {
  $("#editor1").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
$(document).ready( function() {
  $("#editor2").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function () {
        readURL(this);
    });

</script>

@stop
@extends('mhs.maha')
@section('title', 'Tambah Post')

@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
    <div class="col-sm-12" style="margin-top:15px;">
       <p  style="display:inline-block;"><a href="#">Home</a></p><span style="margin-left:5px;margin-right:5px;">>></span><p style="display:inline-block"><a href="#">Tambah Post</a></p>
    </div>
    {!! Form::open(array('route'=>'sAdd','files'=>true)) !!}
    <input type="hidden" name="bhs" value="{{$bhs}}">
       <div class="col-md-6">
           <div class="box-header">
               <h3>TAMBAH POST VERSI BAHASA INDONESIA</h3>
               <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
               <div class="form-group">
                  <label for="judulPost">Judul Post<sup style="color:red">*</sup></label>

                  <input type="text" value="{{old('judulId')}}" required class="form-control" name="judulId" id="jdlPost" placeholder="Judul Post">
               </div>
               <div class="box-body pad" style="padding:0px">
                   <label for="judulPost">Isi Post<sup style="color:red">*</sup></label>

                               <textarea id="editor1" name="contentId" required rows="10" cols="80">
                                  {{old('contentId')}}
                               </textarea>

               </div>
           </div>
       </div>
       <div class="col-md-6">
           <div class="box-header">
               <h3>TAMBAH POST VERSI BAHASA INGGRIS</h3>
               <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
               <div class="form-group">
                  <label for="judulPost">Title Post<sup style="color:red">*</sup></label>
                  <input type="text" value="{{old('judulEn')}}" required name="judulEn" class="form-control" id="jdlPost" placeholder="Title Post">
               </div>
               <div class="box-body pad" style="padding:0px">
                       <label for="judulPost">Content Post<sup style="color:red">*</sup></label>

                               <textarea id="editor2" required name="contentEn" rows="10" cols="80">
                                  {{old('contentEn')}}
                               </textarea>

               </div>
           </div>
       </div>
 </div>
 <div class="col-sm-12" style="margin-top:15px;">
    <div class="col-sm-3">
       <label for="imgPost" style="display:block">Gambar Post<sup style="color:red">*</sup></label>
       <img src="" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
       <input type="file" required id="gambar" name="gBlog" accept="image/*" data-type='image'><br>
    </div>
    <div class="col-sm-3">
       <label for="judulPost">Kategoti Post<sup style="color:red">*</sup></label>
       <div class="form-group">
         <select name="kategori" class="form-control" id="sel1" required>
           <option value="">--Kategori Post--</option>
           <option value="K0hmps">HMPS</option>
           <option value="K0mkrb">KEAKRABAN</option>
           <option value="K0kuni">KUNJUNGAN</option>
           <option value="K0ppm">PPM</option>
           <option value="K0olga">OLAHRAGA</option>
           <option value="K0klta">KESENIAN</option>
           <option value="K0hdrh">KAJIAN</option>
         </select>
       </div>
    </div>
    <div class="col-sm-3">
      <label for="imgPost" style="display:block">Tanggal kegiatan<sup style="color:red">*</sup></label>
      <input type="date" value="{{old('date')}}" class="form-control" required name="date"><br>
    </div>
 </div>
 <div class="col-sm-12" style="margin-bottom:15px;padding-left:15px;">
   <input type="submit" value="Submit" id='simpan' name="simpan" style="margin-top:20px" class="btn btn-success">
   <button style="margin-top:20px" class="btn btn-danger">Batal</button>
</div>
{{csrf_field()}}
{!! Form::close() !!}
@endsection
