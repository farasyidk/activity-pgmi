@section('js')
<script type="text/javascript">
$(document).ready( function() {
  $("#editor1").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
$(document).ready( function() {
  $("#editor2").summernote({
    height : 150,
    tabsize: 2,
    toolbar: [
    // [groupName, [list of button]]
    ['style', ['style','undo','redo','bold', 'italic', 'underline','fontname',]],
    ['font', ['fontsize','strikethrough', 'superscript', 'subscript']],
    ['table', ['table']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'hr', 'codeview','fullscreen']]
  ]});
});
      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function () {
        readURL(this);
    });
</script>
@stop
@extends('mhs.maha')
@section('title', 'Prestasi')
@section('content')
  <div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
       <div class="col-sm-12" style="margin-top:15px;">
          <p>Post versi berbahasa Indonesia maupun berbahasa inggris wajib diisi. Jika tidak bisa menterjemahkan secara manual, Anda dapat menggunakan Google Translate</p>
       </div>
       {!! Form::open(array('route'=>'add-prestasi','files'=>true)) !!}
         {{csrf_field()}}
         <input type="hidden" name="bhs" value="{{$bhs}}">
          <div class="col-md-6">
                  <h3>TAMBAH POST LOMBA VERSI BAHASA INDONESIA</h3>
                  <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                  <div class="form-group">
                          <label>Nama Lomba / Kegiatan<sup style="color:red">*</sup></label>
                          <input type="text" class="form-control" id="lombaPost" placeholder="Nama Lomba / Kegiatan" required name="lombaId" value="{{old('lombaId')}}">
                  </div>
                  <div class="box-body pad" style="padding:0px">
                      <label for="deskripsiLomba">Deskripsi Lomba / Kegiatan</label>

                                  <textarea id="editor1" name="descId" rows="10" cols="80">
                                    {{old('descId')}}
                                  </textarea>

                  </div>
          </div>
          <div class="col-md-6">
                  <h3>TAMBAH POST LOMBA VERSI BAHASA INGGRIS</h3>
                  <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                  <div class="form-group">
                          <label>Name of Contest / Activity<sup style="color:red">*</sup></label>
                          <input type="text" class="form-control" id="lombaPost" name="lombaEn" required value="{{old('lombaEn')}}" placeholder="Nama Lomba / Kegiatan">
                  </div>
                  <div class="box-body pad" style="padding:0px">
                      <label for="deskripsiLomba">Description of Contest / Activity</label>

                                  <textarea id="editor2" name="descEn" rows="10" cols="80">
                                    {{old('descEn')}}
                                  </textarea>

                  </div>
          </div>

    </div>
    <div class="col-sm-6" style="margin-top:15px;">
      <div class="form-group">
              <label>Nama Peserta Lomba / Kegiatan<sup style="color:red">*</sup></label>
              <input type="text" class="form-control" id="namaPeserta" name="peserta" required value="{{old('pesertaId')}}" placeholder="Nama">
      </div>
       <div class="form-group">
          <label>Sifat Lomba / Kegiatan<sup style="color:red">*</sup></label>
          <select class="form-control" name="sifat" required>
              <option>--Choose the level of contest--</option>
              <option value="Group">Group</option>
              <option value="Individu">Individu</option>
          </select>
      </div>
      <div class="form-group">
          <label>Jenjang Lomba / Kegiatan<sup style="color:red">*</sup></label>
          <select class="form-control" name="jenjang" id="sel1" required>
              <option>--Choose the level of contest--</option>
              <option value="Kecamatan">Kecamatan</option>
              <option value="Kabupaten">Kabupaten</option>
              <option value="Province">Provinsi</option>
              <option value="National">Nasional</option>
              <option value="International">Internasional</option>
          </select>
      </div>
      <div class="form-group">
          <label style="display:block">Foto Kegiatan<sup style="color:red">*</sup></label>
          <img src="" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
          <input type="file" required id="gambar" name="gBlog" accept="image/*" data-type='image'><br>
      </div>
      <div class="form-group">
          <label style="display:block">Tanggal Lomba<sup style="color:red">*</sup></label>
          <input type="date" class="form-control" name="tanggal">
      </div>
      <div class="form-group">
          <label style="display:block">NIM<sup style="color:red">*</sup></label>
          <input type="text" class="form-control" name="nim">
      </div>
    </div>
    <div class="col-sm-12" style="margin-bottom:15px;">
      <input type="submit" style="margin-top:20px" class="btn btn-success" value="Submit">
      <button type="reset" style="margin-top:20px" class="btn btn-danger">Reset</button>
    </div>
    {!!Form::close()!!}
@endsection
