@section('js')
<script type="text/javascript" src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{asset('assets/plugins/datatables/moment.js')}}"></script>
<script>

  var oTable;
  $(function () {
    oTable = $("#all-post").DataTable({
        dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            "<'row'<'col-xs-12't>>"+
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url("/dataAll-post") }}',
            data: function (d) {
                d.bahasa = $('input[name=bahasa]:checked').val();
                d.kategori = $('input[name=kategori]:checked').val();
            }
        },
        "fnCreatedRow": function (row, data, index) {
			$('td', row).eq(0).html(index + 1); },
        columns: [
        {data: 'id', name: 'id'},
        {data: 'judul', name: 'judul'},
        {data: 'created_at'},
        {data: 'bahasa'},
        {data: 'nama', name: 'nama'},
        {data: 'aktif', name: 'status', orderable: false, searchable: false},
        {data: 'id', orderable: false, searchable: false, 'mRender': function(data) {
            return '<button onClick="edit('+data+')" class="btn btn-primary btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></button> <button onClick="hapus('+data+')" class="btn btn-primary btn-edit"><i class="fa fa-trash" aria-hidden="true"></i></button>';
        }}, ],
        columnDefs: [
          { targets: 2, render:function(data){
            return moment(data).format('DD-MM-YYYY'); }},
          { targets: 3, render:function(data){
            var bhs;
            if (data == 0) {
              bhs = "Indonesia";
            } else {
              bhs = "Inggris";
            }
            return bhs; }},
          { targets: 5, render: function(data){
            var out;
            if (data == 1) {
              out = "aktif";
            } else if (data == 0 || data == 2) {
              out = "pending";
            } else {
              out = "tolak";
            }
            return out }},
        ]
    });
    $('.searchAjx').click(function(e) {
        oTable.draw();
        e.preventDefault();
    });

  });
    function edit(post) {
      window.location.href="/"+$('input[name=bhs]').val()+"/mhs/edit-post/"+post;
    }
    function hapus(post) {
        confirm('Yakin ingin dihapus?');
        window.location.href="/"+$('input[name=bhs]').val()+"/mhs/destroy-post/"+post;
        /*$.ajax({
            url : '/mhs/destroy-post',
            type : 'get',
            data : {'hapus': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val(),'hps':$('input[name=hps]').val()},
            success: function(data) {
                oTable.draw();
                e.preventDefault();
            }
        });*/

    }
</script>
@stop
@extends('mhs.maha')
@section('title', 'Post')
@section('tag', 'Semua Post')
@section('content')
        <h3 style="padding-left:0px; padding-top:0">ALL POST</h3>
       <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
        <div class="box" style="padding-bottom:10px;border:none;box-shadow:none;margin-top:0px">
                <div class="box-header" style="padding-left:0px; text-align:left">
                    <label><b>Pencarian Menurut :</b></label>
                    {!! Form::open(['url'=>'']) !!}
                    {{csrf_field()}}
                    <div class="form-group" >
                         <label class="radio-inline" style="cursor:text;padding-left:0px">Pilih Kategori<sup style="color:red">*</sup> :</label>
                         @foreach ($category as $cat)
                            <label class="radio-inline">{{ Form::radio('kategori', $cat->id_kategori) }}{{$cat->nama}}</label>
                         @endforeach
                    </div>
                    <div class="form-group">
                         <label class="radio-inline" style="cursor:text;padding-left:0px">Pilih Bahasa<sup style="color:red">*</sup> :</label>
                         <label class="radio-inline">{{ Form::radio('bahasa', 0) }}Bahasa Indonesia {{old('bahasa')}}</label>
                         <label class="radio-inline">{{ Form::radio('bahasa', 1) }}Bahasa Inggris</label>
                    </div>
                    <button class="btn btn-info searchAjx" type="button"><i class="fa fa-search" aria-hidden="true" style="margin-right:5px"></i><b>Search</b></button>
                    <input class="btn btn-danger" type="reset" name="reset" value="Reset">
                    <button type="button" class="btn btn-default pull-right" onClick="window.location.href='/{{$bhs}}/mhs/add-post'"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Post</button>
                    <input type="hidden" name="bhs" value="{{$bhs}}">
                    {!! Form::close() !!}
                </div>
         </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <div class="post-all">
                    @include('mhs.data-all-post')
                </div>
            </div>
            <!-- /.box-body -->
          </div>
@endsection
