@extends('layouts.master')
@section('title', 'Cara Upload')
@section('content')
<div id="single-post">
<div id="single-post-inner" class="container">
<div class="col-md-9 list-into-single">
    <div>
        <p class="list-page-single"><a href="/">{{$navs[0]['nav']}}</a></p>>><p class="list-page-single"><a href="#">{{$navs[3]['nav']}}</a></p>
    </div>
</div>
<div class="col-md-9 single-post-posts" style="padding-left:35px;padding-right:15px;padding-bottom:30px">

    <div id="title-post" style="margin-bottom:20px">
        <h2>Cara Upload</h2>
        <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
    </div>

        <b>Bagaimana cara upload?</b>
        <li> Klik tombol “Login”</li>
        <li> Masuk menggunakan akun NIM dan Password Anda. Jika belum memiliki akun, Anda bisa menghubungi admin prodi PGMI.</li>
        <li> Buat pos. Caranya? klik menu "Post" di sidebar.</li>
        <li> Disitu akan ditampilkan post-post yang pernah Anda buat. Untuk membuat post, klik tombol " + Tambah Post "</li>
        <li> Naskah materi post wajib diketik dengan dua versi yaitu versi bahasa Indonesia dan versi bahasa Inggris. Jika sulit menterjemahkan secara manual, Anda dapat meminta bantuan Google Translate.</li>
        <li> Setelah naskah materi anda telah selesai. Masukkan gambar untuk post tersebut, hukumnya wajib. Dan jangan lupa untuk memilih kategori post tersebut.</li>
        <li> Setelah selesai. Klik tombol “Submit”. </li>
        <br>
        <b>Bagaimana cara melihat status post Anda?</b>
        <li>Klik Menu “Post”</li>
        <li>Maka Anda akan melihat semua post yang pernah Anda buat, beserta statusnya. Jika status " Pending ", berarti masih dalam proses moderasi dari pihak admin. Jika status " Diterima ", maka post Anda telah disetujui oleh Admin, dan telah terbit di halaman web. Jika status " Ditolak ", maka post Anda tidak disetujui oleh Admin.</li>

        <div id="title-post" style="margin-bottom:20px">
            <h2>GUIDE UPLOAD</h2>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
        </div>

            <b>How to upload??</b>
            <li> Click the "Login" button</li>
            <li> Sign in with your NIM and Password account. If you do not have an account yet, you can contact the PGMI prodi admin.</li>
            <li> Create a post. How? click the "Post" menu in the sidebar.</li>
            <li> There will be post-post you will ever create. To create a post, click the "+ Add Post" button "</li>
            <li> The post material script must be typed in two versions, the Indonesian version and the English version. If it's difficult to manually translate, you can request Google Translate help.</li>
            <li> After your manuscript has finished. Enter the image for the post, the law is mandatory. And do not forget to select the category of the post.</li>
            <li> After it finishes. Click the "Submit" button. </li>
            <br>
            <b>How do I view your post status?</b>
            <li>Click on the "Post" menu</li>
            <li>Then you'll see all the posts you've ever created, along with their status.
                If the status of "Pending", means still in the process of moderation from the admin.
                If the status "Accepted", then your post has been approved by the Admin,
                and has appeared on the web page. If the status is "Disapproved", your post is not approved
                by Admin.</li>
</div>
<div id="wrap-sidebar-single" class="col-md-3">
    @include("layouts.sidebar")
</div>
</div>
</div>
@endsection
