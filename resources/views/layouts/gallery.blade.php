@extends('layouts.master')
@section('title', 'Galleri')
@section('content')

<div id="gallery-wrap-out">
  <div id="gallery-wrap" class="container">

      <div id="title-list-posts-wrap">
          <h2 class="title-section">{{$navs[8]['nav']}}</h2>
          <div class="underscore"></div>
      </div>

      <div align="center">
          <button class="btn btn-default filter-button" data-filter="all">{{$bhs == 'id' ? 'Semua' : 'All'}}</button>
          @for($a=0; $a< count($kategori);$a++)
              <button class="btn btn-default filter-button" data-filter="{{$kategori[$a]['id_kategori']}}">{{$kategori[$a][$bhs=='id' ? 'nama':'namaEn']}}</button>
          @endfor

      </div>
      <br/>
      <div id="gallery-wrap-inner" class="tz-gallery">
        @for ($e=0; $e < count($id); $e++)
        @foreach ($vImage as $v)
          @if ($v->id_gallery == $id[$e])
          <div class="gallery_product filter {{$v->kategori}}">
              <a target="_blank" href="{{asset('assets/img/blogs/'.$v->gambar)}}">
                  <img src="{{asset('assets/img/blogs/'.$v->gambar)}}" class="img-responsive img-gallery">
              </a>
          </div>
        @endif
          @endforeach
        @endfor
      </div>
  </div>
</div>
@endsection
<!--
<div align="center">
    <button class="btn btn-default filter-button" data-filter="all">Semua</button>
    @foreach ($gallery as $gall)
      <button class="btn btn-default filter-button" data-filter="{{$gall->id_kategori}}">{{$gall->nama}}</button>
    @endforeach
</div>
<br/>
<div id="gallery-wrap-inner" class="tz-gallery">
  @foreach ($gallery as $galle)
    <div class="gallery_product filter {{$galle->id_kategori}}">
        <a target="_blank" href="{{asset('assets/img/'.$galle->gambar)}}">
            <img src="{{asset('assets/img/'.$galle->gambar)}}" class="img-responsive img-gallery">
        </a>
    </div>
    @endforeach
</div>
-->
