@foreach ($blogs as $blog)
@php if (!empty($blog->gol)) $url = $blog->gol; else $url = $blog->nama; @endphp
<div class="panel-post-wrap">
    <div class="col-sm-4 img-list-posts-wrap">
        <img src="{{asset('assets/img/blogs/'.$blog->gambar)}}" class="img-responsive img-list-posts"/>
    </div>
    <div class="col-sm-8">
        <h3 class="title-isi-list-posts"><a href="/{{$bhs}}/{{$url}}/{{$blog->slug}}">{{$blog->judul}}</a></h3>
        <div class="detail-post detail-post-list-posts">
            <p class="date-post">
                <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[16]['nav']}} :</b>
                <span class="text-date-post">{{date('d M Y', strtotime($blog->created_at))}}</span>
            </p>
            <p class="created-post">
                <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[17]['nav']}} : </b>
                <span class="text-created-post">{{$blog->from}}</span>
            </p>
        </div>
        <div class="isi-lists-posts">
            <article>{{ str_limit(strip_tags($blog->content), 200) }}</p>
        </div>
        <a href="/{{$bhs}}/{{$url}}/{{$blog->slug}}" type="button" class="btn btn-success">{{$navs[12]['nav']}}</a>
    </div>
</div>
@endforeach
<div class="col-sm-12 pagination-wrap">
    {!! $blogs->render() !!}
<!--<ul class="pagination pagination-list-posts">
<li><a href="#">Previous</a></li>
<li class="active"><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#">Next</a></li>
</ul> -->
</div>
