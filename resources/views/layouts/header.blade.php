<nav id="navbar-outer-up" class="navbar navbar-inverse">
   <div class="container">
    <div id="nav-header-up" class="navbar-header">
      <div id="navbar-title" class="navbar-brand" href="#" style="padding-top:11px"><i class="glyphicon glyphicon-education icon-navbar-up"></i><b>{{$navs[10]['nav']}}</b>
            <ul class="nav navbar-nav navbar-right menu-nav-bottom">
                <li>
                    <div class="dropdown flag-dropdown">
                    <button class="btn dropdown-toggle flag-btn" type="button" data-toggle="dropdown"><span class="text-flag"><img class="flag" src="{{asset('assets/img/'.flagActive())}}" />{{bhsActive()}}</span>
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-flag">
                        <li class="{{set_active('id')}}"><a href="javascript:void(0)"><h5><img class="flag" src="{{asset('assets/img/ind.svg')}}" />Indonesia</h5></a></li>
                        <li class="{{set_active('en')}}"><a href="javascript:void(0)"><h5><img class="flag" src="{{asset('assets/img/uk.svg')}}" />English</h5></a></li>
                    </ul>
                  </div>
                </li>
            </ul>
        </div>
    </div>
   </div>
</nav>
<nav id="navbar-outer-bottom" class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a id="navbar-title-bottom"class="navbar-brand" href="#"><img id="logo" src="{{asset('assets/img/uu.png')}}"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav menu-nav-bottom">
          <li class="{{navActive(["en","id","/"])}}"><a href="/{{$bhs}}"><i class="glyphicon glyphicon-home"></i>{{$navs[0]['nav']}}</a></li>
          <li class="{{navActive(['hmps','HMPS'])}}"><a href="/{{$bhs}}/hmps">{{$navs[1]['nav']}}</a></li>
          <li class="{{navActive('cara-upload')}}"><a href="/{{$bhs}}/cara-upload">{{$navs[3]['nav']}}</a></li>
          <li class="dropdown {{navActive(['Kegiatan','kegiatan'])}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{$navs[2]['nav']}}
            <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/{{$bhs}}/kegiatan/keakraban">{{$navs[21]['nav']}}</a></li>
              <li><a href="/{{$bhs}}/kegiatan/kunjungan">{{$navs[5]['nav']}}</a></li>
              <li><a href="/{{$bhs}}/kegiatan/ppm">PPM</a></li>
            </ul>
          </li>
          <li class="dropdown {{navActive(['Ekstra','ekstra'])}}">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{$navs[4]['nav']}}
            <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/{{$bhs}}/ekstra/olahraga">{{$navs[6]['nav']}}</a></li>
              <li><a href="/{{$bhs}}/ekstra/kesenian">{{$navs[7]['nav']}}</a></li>
              <li><a href="/{{$bhs}}/ekstra/kajian">{{$navs[22]['nav']}}</a></li>
            </ul>
          </li>
          <li class="dropdown {{navActive('gallery')}}"><a href="/{{$bhs}}/gallery">{{$navs[8]['nav']}}</a></li>
          <li class="dropdown {{navActive('prestasi')}}"><a href="/{{$bhs}}/prestasi">{{$navs[20]['nav']}}</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right menu-nav-bottom nav-log-search">
          @guest
            <!--<li class="{{navActive("login")}}"><a href="/{{$bhs}}/login" id="login-navbar"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
          @else
            <!--<li><a href="/mhs/{{$bhs}}" id="login-navbar"><span class="glyphicon glyphicon-user" style="margin-right:5px"></span>MAHASISWA</a></li>-->
          @endguest
          <li class="li-form">
                <!--  SEARCH -->
                <form role="search" id="search-nav" method="get" action="/{{$bhs}}/result">
                    <input type="search" class="search-field" autofocus="autofocuse" placeholder="{{$navs[9]['nav']}} …" name="search" title="Rechercher :">
                    <button type="reset">
                            <span class="glyphicon glyphicon-remove-circle">
                                <span class="sr-only">Close</span>
                            </span>
                    </button>
                    <button type="submit" class="search-submit">
                            <span class="glyphicon glyphicon-search"><a id="search-text" href="#">{{$navs[9]['nav']}}</a>
                                <span class="sr-only">Rechercher</span>
                            </span>
                    </button>
                </form>
            </li>
        </ul>
      </div>
    </div>
  </nav>
