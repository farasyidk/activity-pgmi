@extends('layouts.master')
@section('title', 'Home')

@section('content')
<script>
    $(document).ready(function(){
        $(".main-pengumuman img").hide();
        $("p img").hide(); $("p blockquote").disabled();
    });
</script>
    <div id="carousel-wrap">
        <div id="carousel-wrap-in" class="container">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner slider-post">
                  @php $i = 0; @endphp
                  @for ($u = 0; $u<count($slider);$u++)
                <div class="item @if ($i <=0) {{'active'}} @endif">
                  <img src="assets/img/blogs/{{$slider[$u]['gambar']}}" alt="...">
                  <div class="carousel-caption">
                    <h2>{{$slider[$u][$cpt]}}</h2>
                  </div>
                </div>
                @php $i++; @endphp
                @endfor
              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
          </div>
    </div>
    <div id="popular-wrap">
    </div>
    <div id="popular-wrap-bottom">
      <h2 class="title-section">{{$navs[11]['nav']}}</h2>
      <div class="underscore"></div>
      <div class="container">
          @foreach ($popBlogs as $pop)
              @php if (!empty($pop->gol)) $pUrl = $pop->gol; else $pUrl = $pop->nama; @endphp
         <div data-aos="fade-up">
            <div class="col-sm-4 post-populer">
              <img class="img-responsive img-popular" src="assets/img/blogs/{{$pop->gambar}}">
              <h3>
                <a href="{{$bhs}}/{{$pUrl}}/{{$pop->slug}}" class="title-post-popular">{{$pop->judul}}</a>
              </h3>
              <article>
                  {{ str_limit(strip_tags($pop->content), 250) }}
              </article>
              <a href="{{$bhs}}/{{$pUrl}}/{{$pop->slug}}" style="color:green">{{$navs[12]['nav']}} >> </a>
            </div>
         </div>
         @endforeach
        </div>
    </div>
    <div class="col-sm-12" id="activity">
      <div class="container">
        <h2 class="title-section" style="color:#fff;margin-top:30px">{{$navs[13]['nav']}}</h2>
          <div class="underscore"></div>
                    <div class="col-md-12">
                        <div id="news-slider" class="owl-carousel">
                            @foreach ($extBlogs as $extBlog)
                            <div data-aos="zoom-in-up">
                                    <div class="post-slide">
                                        <div class="post-img">
                                            <a href="{{$bhs}}/Ekstra/{{$extBlog->slug}}"><img src="assets/img/blogs/{{$extBlog->gambar}}" alt=""></a>
                                        </div>
                                        <div class="post-content">
                                            <div class="post-date">
                                                <span class="month">{{date('M', strtotime($extBlog->created_at))}}</span>
                                                <span class="date">{{date('d', strtotime($extBlog->created_at))}}</span>
                                            </div>
                                            <h5 class="post-title"><a href="{{$bhs}}/Ekstra/{{$extBlog->slug}}">{{$extBlog->judul}}</a></h5>
                                            <article>{{ str_limit(strip_tags($extBlog->content), 200) }} </article>
                                        </div>
                                        <ul class="post-bar">
                                            <li>{{$navs[14]['nav']}} : <a href="{{$bhs}}/{{$extBlog->nama}}">{{$extBlog->$kat}}</a> </li>
                                        </ul>
                                    </div>
                            </div>
                            @endforeach
                        </div>
                    </div>


      </div>
    </div>
    <div id="all-post-wrapper">
        <div class="container-fluid">
            <div class="col-sm-3">
                <h3 class="title-section title-section-bottom">{{$navs[19]['nav']}}</h3>
                <div class="underscore"></div>
                @foreach ($aktifBlogs as $aktif)
                <div data-aos="zoom-in">
                    <div class="col-sm-12 post-pengumuman">
                        <div class="col-sm-12 main-pengumuman">
                            <a href="#">
                                <h3 class="title-pengumuman">{{$aktif->from}}</h3>
                                <h6>Total Post: {{$aktif->maha}}</h6>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div id="recent-post" class="col-sm-6">
                <h3 class="title-section title-section-bottom">{{$navs[15]['nav']}}</h3>
                <div class="underscore"></div>
                @foreach ($newBlogs as $newBlog)
                    @php if (!empty($newBlog->gol)) $nUrl = $newBlog->gol; else $nUrl = $newBlog->nama; @endphp
                    <div data-aos="zoom-in">
                        <div class="col-sm-12 post-recent">
                            <div class="col-sm-4 col-xs-5 img-recent-wrap">
                                <img class="img-responsive img-recent" src="assets/img/blogs/{{$newBlog->gambar}}"/>
                            </div>
                            <div class="col-sm-8 col-xs-7 main-pengumuman">
                                <a href="{{$bhs}}/{{$nUrl}}/{{$newBlog->slug}}">
                                    <h5 class="title-recent">{{$newBlog->judul}}</h5>
                                </a>
                                <article>
                                    {{ str_limit(strip_tags($newBlog->content), 250) }}
                                </article>
                                <a href="{{$bhs}}/{{$nUrl}}/{{$newBlog->slug}}" style="color:green">{{$navs[12]['nav']}} >> </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-sm-3">
                <h3 class="title-section title-section-bottom">{{$navs[20]['nav']}}</h3>
                <div class="underscore"></div>
                <div class="col-sm-12 agenda-wrapper">
                    <ul>
                    @foreach ($events as $event)
                        <div data-aos="zoom-in">
                            <li class="agenda-post"><a href="{{$bhs}}/Prestasi/{{$event->slug}}" style="text-decoration: none; color: #2ACC6D">{{str_limit($event->judul, 20)}}</a><p class="date-agenda">{{$event->peserta}}</p></li>
                        </div>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
