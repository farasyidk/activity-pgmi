@extends('layouts.master')

@section('title', $post[0]['judul'])
@section('content')
<div id="single-post">
   <div id="single-post-inner" class="container">
       <div class="col-md-9">
           <div id="list-into">

               <p class="list-page"><a href="/{{$bhs}}">{{$navs[0]['nav']}}</a></p>
               @if (!empty($post[0]['nama']))
                  >><p class="list-page"><a href="/{{$bhs}}/{{$url}}">{{$post[0][$bhs == 'id' ? 'nama' : 'namaEn']}}</a></p>
               @endif
           </div>
           <div id="title-post">
               <h2>{{$post[0]['judul']}}</h2>
           </div>
           <div id="detail-post">
               <p id="date-post">
                   <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[16]['nav']}} :</b>
                   <span id="text-date-post">{{date('d M Y', strtotime($post[0]['tanggal']))}}</span>
                  &nbsp;
                   <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[17]['nav']}} : </b>
                   <span id="text-created-post">{{$post[0]['from']}}</span>
               </p>
           </div>
           <div id="img-post-wrap">
               <img class="img-responsive img-post" src="{{asset('assets/img/b.jpeg')}}"/>
           </div>
           <p id="isi-post">{!!$post[0]['content']!!}</p>
           <div id="post-bottom-wrap">
               <div data-aos="zoom-up">
                   <div id="terkait-post" class="col-sm-6">
                       <h3>{{$navs[23]['nav']}}</h3>
                       <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                       <ul id="terkait-post-list">
                          @foreach ($terkaits as $terkait)
                              @php if (!empty($terkait->gol)) $url = $terkait->gol; else $url = $terkait->nama; @endphp
                              <li><a href="/{{$bhs}}/{{$url}}/{{$terkait->slug}}">{{$terkait->judul}}</a></li>
                          @endforeach
                      </ul>
                   </div>
               </div>
               <div data-aos="zoom-up">
                   <div id="terbaru-post" class="col-sm-6">
                       <h3>{{$navs[15]['nav']}}</h3>
                       <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
                       <ul id="terbaru-post-list">
                         @foreach ($newPost as $new)
                            @php if (!empty($new->gol)) $url = $new->gol; else $url = $new->nama; @endphp
                           <li><a href="/{{$bhs}}/{{$url}}/{{$new->slug}}">{{$new->judul}}</a></li>
                         @endforeach
                       </ul>
                   </div>
               </div>
           </div>
       </div>
       <div class="col-md-3">
          @include("layouts.sidebar")
       </div>
   </div>
 </div>
@endsection
