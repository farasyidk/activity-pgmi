@extends('layouts.master')
@section('title', 'Login')
@section('content')
<div id="login-wraps">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <img src="{{asset('assets/img/logo-uin.png')}}" class="img-login"/>
      <h2 id="title-login">UIN SUNAN KALIJAGA</h2>
        <h5 id="sub-title-login">PENDIDIKAN GURU MADRASAH IBTIDAIYAH</h5>
      <div class="box">
          <form class="" action="/mhs/{{$bhs}}" method="get">
              <input class="input-login" type="text" placeholder="username">
              <input class="input-login" type="password" placeholder="password">
              <button class="btn btn-default full-width"><span class="glyphicon glyphicon-ok" style="margin-right:5px;"></span><b>SUBMIT</b></button>
          </form>
      </div>
    </div>
    <div class="col-sm-4"></div>
</div>
@endsection
