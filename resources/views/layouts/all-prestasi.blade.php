@extends('layouts.master')
@section('title', 'Prestasi')
@section('content')
<div class="container">
    <div class="col-md-9 list-into-single">
        <div>
            <p class="list-page-single"><a href="#">Beranda</a></p>>><p class="list-page-single"><a href="#">Prestasi</a>
        </div>
    </div>
    <div class="col-md-9 single-post-posts">

        <div id="title-post">
            <h2>{{$post[0]['judul']}}</h2>
        </div>
        <div class="detail-post">
            <p class="date-post">
                <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>Tanggal :</b>
                <span class="text-date-post">{{date('d M Y', strtotime($post[0]['tanggal']))}}</span>
            </p>
            <p class="created-post">
                <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>Ditulis oleh : </b>
                <span class="text-created-post">16650065</span>
            </p>
        </div>
        <div id="img-post-wrap">
            <img id="img-sertifikat" class="img-responsive img-post" src="{{asset('assets/img/blogs/download.jpg')}}"/>
        </div>
        <div id="isi-post" style="font-family:'Montserrat',Helvetica,Arial,sans-serif;">
            <p><b>Nama Peserta : </b><span>{{$post[0]['peserta']}}</span></p>
            <p><b>Sifat Lomba / Kegiatan : </b><span>{{$post[0]['sifat']}}</span></p>
            <p><b>Jenjang Lomba / Kegiatan : </b><span>{{$post[0]['jenjang']}}</span></p>
            <p><b>Deskripsi Lomba / Kegiatan: </b><br /><span>{!!$post[0]['content']!!}</span></p>
        </div>
        <div id="post-bottom-wrap">
 <div data-aos="zoom-up">
     <div id="terkait-post" class="col-sm-6">
         <h3>POST TERKAIT</h3>
         <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
         <ul id="terkait-post-list">
            @foreach ($terkaits as $terkait)
                <li><a href="#">{{$terkaits[0]['judul']}}</a></li>
            @endforeach
        </ul>
     </div>
 </div>
 <div data-aos="zoom-up">
     <div id="terbaru-post" class="col-sm-6">
         <h3>POST TEBARU</h3>
         <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
         <ul id="terbaru-post-list">
           @foreach ($newPost as $new)
              @php if (!empty($new->gol)) $url = $new->gol; else $url = $new->nama; @endphp
             <li><a href="/{{$bhs}}/{{$url}}/{{$new->slug}}">{{$new->judul}}</a></li>
           @endforeach
         </ul>
     </div>
 </div>
</div>
    </div>
    <div id="wrap-sidebar-single" class="col-md-3">
        @include("layouts.sidebar")
    </div>
</div>
@endsection
