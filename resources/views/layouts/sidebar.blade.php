<div id="populer-wrap" class="col-md-12 col-sm-6">
    <div class="title-sidebar">
      <h4>
          <span class="glyphicon glyphicon-file"></span>
          <b >POST</b> POPULER
      </h4>
      <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
    </div>

    <div id="post-populer-sidebar">
      <ul id="post-populer-sidebar-list">
          @foreach ($popBlogs as $popBlog)
              @php if (!empty($popBlog->gol)) $url = $popBlog->gol; else $url = $popBlog->nama; @endphp
              @if (!empty($popBlog->gol))
                  <li><a href="/{{$bhs}}/{{$url}}/{{$popBlog->slug}}"><span class="glyphicon glyphicon-file" style="margin-right:5px"></span>{{str_limit($popBlog->judul, 23)}}</a></li>
              @else
                  <li><a href="/{{$bhs}}/{{$url}}/{{$popBlog->slug}}"><span class="glyphicon glyphicon-file" style="margin-right:5px"></span>{{str_limit($popBlog->judul, 23)}}</a></li>
              @endif
          @endforeach
      </ul>
    </div>
</div>
<div id="agenda-wrap" class="col-md-12 col-sm-6">
    <div class="title-sidebar" style="margin-top:15px;">
        <h4>
            <span class="glyphicon glyphicon-calendar"></span>
            {{$navs[19]['nav']}}
        </h4>
        <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
    </div>
    @foreach ($aktifBlogs as $aktif)
    <div data-aos="zoom-in">
        <div class="col-sm-12 post-pengumuman">
            <div class="col-sm-12 main-pengumuman">
                <a href="#">
                    <h3 class="title-pengumuman">{{$aktif->from}}</h3>
                    <h6>Total Post: {{$aktif->maha}}</h6>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
