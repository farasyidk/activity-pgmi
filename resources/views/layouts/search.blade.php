@extends('layouts.master')
@section('title', 'Search')
@section('content')
<div id="list-post-wrap">
    <div class="container">
      <div class="col-md-9">
          <div id="list-into">
            <p class="list-page"><a href="/{{$bhs}}">{{$navs[0]['nav']}}</a></p>>>
            <p class="list-page"><a href="#">Search</a></p>
          </div>
          <div id="title-list-posts-wrap">
              <h2 class="title-section" style="text-align:left">Search</h2>
              <div class="underscore" style="margin-left:0px;margin-right:0px;"></div>
          </div>
          <div class="blogss">
          
          @if (!empty($blogs))
          @foreach ($blogs as $blog)
          @php if (!empty($blog->gol)) $url = $blog->gol; else $url = $blog->nama; @endphp
          <div class="panel-post-wrap">
              <div class="col-sm-4 img-list-posts-wrap">
                  <img src="{{asset('assets/img/blogs/'.$blog->gambar)}}" class="img-responsive img-list-posts"/>
              </div>
              <div class="col-sm-8">
                  <h3 class="title-isi-list-posts"><a href="/{{$bhs}}/{{$url}}/{{$blog->slug}}">{{$blog->judul}}</a></h3>
                  <div class="detail-post detail-post-list-posts">
                      <p class="date-post">
                          <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[16]['nav']}} :</b>
                          <span class="text-date-post">{{date('d M Y', strtotime($blog->created_at))}}</span>
                      </p>
                      <p class="created-post">
                          <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[17]['nav']}} : </b>
                          <span class="text-created-post">{{$blog->from}}</span>
                      </p>
                  </div>
                  <div class="isi-lists-posts">
                      <article>{{ str_limit(strip_tags($blog->content), 200) }}</p>
                  </div>
                  <a href="/{{$bhs}}/{{$url}}/{{$blog->slug}}" type="button" class="btn btn-success">{{$navs[12]['nav']}}</a>
              </div>
          </div>
          @endforeach
      @endif
          @if (empty($blogs2))
              @foreach ($blogs2 as $blog2)
                  @php if (!empty($blog2->gol)) $url = $blog2->gol; else $url = $blog2->nama; @endphp
                  <div class="panel-post-wrap">
                      <div class="col-sm-4 img-list-posts-wrap">
                          <img src="{{asset('assets/img/blogs/'.$blog2->gambar)}}" class="img-responsive img-list-posts"/>
                      </div>
                      <div class="col-sm-8">
                          <h3 class="title-isi-list-posts"><a href="/{{$bhs}}/{{$url}}/{{$blog2->slug}}">{{$blog2->judul}}</a></h3>
                          <div class="detail-post detail-post-list-posts">
                              <p class="date-post">
                                  <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[16]['nav']}} :</b>
                                  <span class="text-date-post">{{date('d M Y', strtotime($blog2->created_at))}}</span>
                              </p>
                              <p class="created-post">
                                  <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>{{$navs[17]['nav']}} : </b>
                                  <span class="text-created-post">{{$blog2->from}}</span>
                              </p>
                          </div>
                          <div class="isi-lists-posts">
                              <article>{{ str_limit(strip_tags($blog2->content), 200) }}</p>
                          </div>
                          <a href="/{{$bhs}}/{{$url}}/{{$blog2->slug}}" type="button" class="btn btn-success">{{$navs[12]['nav']}}</a>
                      </div>
                  </div>
              @endforeach
          @endif
          <div class="col-sm-12 pagination-wrap">

          <!--<ul class="pagination pagination-list-posts">
          <li><a href="#">Previous</a></li>
          <li class="active"><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">Next</a></li>
          </ul> -->
          </div>

          </div>
      </div>
      <div class="col-md-3">
          @include('layouts.sidebar')
      </div>
    </div>
</div>
@endsection
