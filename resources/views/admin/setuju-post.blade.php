@section('js')
  <script type="text/javascript">
  function confirmed(post) {
    window.location.href="/adminn/confirmed/"+post;
  }
  function destroy(post) {
    if (confirm('yakin ingin dihapus?')) {
      $.ajax({
          url : '/adminn/destroy-post/',
          type : 'post',
          data : {'hapus': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val(),'hps':$('input[name=hps]').val()},
          success: function(data) {

              $('.post-all').empty().html(data);
              $('.cukses').show();
              $('#message').html('Post Berhasil dihapus');
          }
      });
    }
  }
  </script>
@stop
@extends('admin.admin')
@section('title', 'Persetujuan Post')
@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:10px">
    <div class="col-sm-12">
      <div class="box" style="padding-bottom:10px;border:none;box-shadow:none">
        <div class="box-header" style="padding-left:0px">
          <h1 class="box-title" style="display:block;font-weight:bold;font-size:2.3em">PERSETUJUAN POST</h1>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
        </div>
        <div class="alert alert-success cukses" hidden="hidden">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong id="message"></strong>
        </div>
        <!-- /.box-header -->
        <div class="post-all">
            @include('admin.data-setuju-post')
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>

</div>
@endsection
