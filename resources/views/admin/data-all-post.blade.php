@if ($message = Session::get('success'))
<div class="alert alert-success sukses">
    <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{$message}}</strong>
</div>
@endif
<div class="box-body table-responsive no-padding">
    <table id="all-post" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Tanggal</th>
            <th>Kategori</th>
            <th>Action</th>
        </tr>
        </thead>
      </table>
    </div>