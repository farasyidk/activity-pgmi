@section('js')
  <script type="text/javascript">
  function destroy(post) {
    if (confirm('yakin ingin dihapus?')) {
      $.ajax({
          url : '/adminn/destroy-prestasi/',
          type : 'post',
          data : {'data': post,'_method':$('input[name=_method]').val(),'_token':$('input[name=_token]').val()},
          success: function(data) {
              $('.post-all').empty().html(data);
              $('.sukses').show();
              $('#message').html('Post Berhasil dihapus');
          }
      });
    }
  }
  </script>
@stop
@extends('admin.admin')
@section('title', 'Prestasi')
@section('content')
<div class="box box-info" style="border-top-color: #e9ef00;box-shadow:none;margin-top:0px;padding-bottom:20px">
    <div class="col-sm-12">
      <div class="box" style="padding-bottom:10px;border:none;box-shadow:none">
        <div class="box-header" style="padding-left:0px">
          <h1 class="box-title" style="display:block;font-weight:bold;font-size:2.3em">PRESTASI POST</h1>
            <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <button type="button" onclick="window.location.href='/adminn/add-prestasi'" class="btn btn-default"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Prestasi Post</button>
            </div>
          </div>
        </div>
        <div class="alert alert-success sukses" hidden="hidden">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong id="message"></strong>
        </div>
        @if ($message = Session::has('message'))
        <div class="alert alert-success sukses">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{$message}}</strong>
        </div>
        @endif
        <!-- /.box-header -->
        <div class="post-all">
            @include('admin.data-prestasi')
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
@endsection
