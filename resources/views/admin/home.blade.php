@extends('admin.admin')
@section('title', 'Dashboard')

@section('content')
<section class="content-header">
      <h1>
            Dashboard
            <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
      </ol>
</section>

<div class="row" style="padding-left:15px;padding-right:15px">
      <div class="alert alert-success alert-dismissable" style="background-color:rgba(124, 231, 129, 0.92) !important;padding-top:10px;padding-bottom:5px;border:none">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4><strong>Success, </strong>Selamat Datang Admin ! </h4>
      </div>
</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
  <!-- Left col -->

  <!-- /.Left col -->
  <!-- right col (We are only adding the ID to make the widgets sortable)-->

  <!-- right col -->
</div>
<!-- /.row (main row) -->
@endsection
